﻿namespace NfeSefazXmlImporter
{
    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.portalfiscal.inf.br/nfe")]
    public partial class nfeProcNFeInfNFeDetImpostoCOFINS
    {

        private nfeProcNFeInfNFeDetImpostoCOFINSCOFINSAliq cOFINSAliqField;

        /// <remarks/>
        public nfeProcNFeInfNFeDetImpostoCOFINSCOFINSAliq COFINSAliq
        {
            get
            {
                return this.cOFINSAliqField;
            }
            set
            {
                this.cOFINSAliqField = value;
            }
        }
    }


}

﻿using System;
using System.ComponentModel;
using System.Xml.Serialization;

namespace NfeUtils.NfeClasses
{
    [Serializable]
    [DesignerCategory("code")]
    [XmlType(AnonymousType = true, Namespace = "http://www.w3.org/2000/09/xmldsig#")]
    public class SignatureSignedInfoCanonicalizationMethod
    {

        [XmlAttribute]
        public string Algorithm { get; set; }
    }


}

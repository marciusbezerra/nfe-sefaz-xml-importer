﻿using System;
using System.ComponentModel;
using System.Xml.Serialization;

namespace NfeUtils.NfeClasses
{
    [Serializable]
    [DesignerCategory("code")]
    //[XmlType(AnonymousType = true, Namespace = "http://www.portalfiscal.inf.br/nfe")]
    public class nfeProcNFeInfNFepagdetPagcard
    {
        public string tpIntegra { get; set; }
        public string CNPJ { get; set; }
        public string tBand { get; set; }
        public string cAut { get; set; }
    }
}
﻿namespace NfeUtils.NfeClasses
{
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    ////[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.portalfiscal.inf.br/nfe")]
    public partial class nfeProcNFeInfNFeDetImpostoCOFINS
    {

        public nfeProcNFeInfNFeDetImpostoCOFINSCOFINS COFINSAliq { get; set; }
        public nfeProcNFeInfNFeDetImpostoCOFINSCOFINS COFINSNT { get; set; }
    }


}

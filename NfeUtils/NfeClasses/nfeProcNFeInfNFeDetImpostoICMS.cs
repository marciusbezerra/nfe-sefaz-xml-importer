﻿using System;
using System.ComponentModel;
using System.Xml.Serialization;

namespace NfeUtils.NfeClasses
{
    [Serializable]
    [DesignerCategory("code")]
    //[XmlType(AnonymousType = true, Namespace = "http://www.portalfiscal.inf.br/nfe")]
    public partial class nfeProcNFeInfNFeDetImpostoICMS
    {

        public nfeProcNFeInfNFeDetImpostoICMSICMS ICMS00 { get; set; }
        public nfeProcNFeInfNFeDetImpostoICMSICMS ICMS20 { get; set; }
        public nfeProcNFeInfNFeDetImpostoICMSICMS ICMS40 { get; set; }
        public nfeProcNFeInfNFeDetImpostoICMSICMS ICMS60 { get; set; }
    }


}

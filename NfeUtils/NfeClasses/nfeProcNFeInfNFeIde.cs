﻿using System.Xml.Serialization;
using System.ComponentModel;
using System;

namespace NfeUtils.NfeClasses
{
    [Serializable]
    [DesignerCategory("code")]
    //[XmlType(AnonymousType = true, Namespace = "http://www.portalfiscal.inf.br/nfe")]
    public partial class nfeProcNFeInfNFeIde
    {
        public string cUF { get; set; }
        public string cNF { get; set; }
        public string natOp { get; set; }

        public bool ShouldSerializeindPag() => !string.IsNullOrWhiteSpace(indPag);
        public string indPag { get; set; }

        public string mod { get; set; }
        public string serie { get; set; }
        public string nNF { get; set; }
        public string dhEmi { get; set; }

        public bool ShouldSerializedhSaiEnt() => !string.IsNullOrWhiteSpace(dhSaiEnt);
        public string dhSaiEnt { get; set; }

        public string tpNF { get; set; }
        public string idDest { get; set; }
        public string cMunFG { get; set; }
        public string tpImp { get; set; }
        public string tpEmis { get; set; }
        public string cDV { get; set; }
        public string tpAmb { get; set; }
        public string finNFe { get; set; }
        public string indFinal { get; set; }
        public string indPres { get; set; }
        public string procEmi { get; set; }
        public string verProc { get; set; }

        public bool ShouldSerializeNFref() => NFref?.refNFe != null;
        public nfeProcNFeInfNFeIdeNFref NFref { get; set; }
    }


}

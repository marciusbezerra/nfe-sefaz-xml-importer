﻿using System;
using System.ComponentModel;
using System.Xml.Serialization;

namespace NfeUtils.NfeClasses
{
    [Serializable]
    [DesignerCategory("code")]
    //[XmlType(AnonymousType = true, Namespace = "http://www.portalfiscal.inf.br/nfe")]
    public class nfeProcNFeInfNFepagdetPag
    {
        public string tPag { get; set; }
        public string vPag { get; set; }

        public bool ShouldSerializecard()
        {
            return card.tpIntegra != null;
        }

        public nfeProcNFeInfNFepagdetPagcard card { get; set; }
        public string vTroco { get; set; }
    }
}
﻿using System;
using System.ComponentModel;

namespace NfeUtils.NfeClasses
{
    [Serializable]
    [DesignerCategory("code")]
    public class SignatureSignedInfo
    {

        public SignatureSignedInfoCanonicalizationMethod CanonicalizationMethod { get; set; }

        public SignatureSignedInfoSignatureMethod SignatureMethod { get; set; }

        public SignatureSignedInfoReference Reference { get; set; }
    }


}

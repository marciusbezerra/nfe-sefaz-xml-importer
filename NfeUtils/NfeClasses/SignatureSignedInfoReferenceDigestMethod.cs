﻿using System.Xml.Serialization;
using System.ComponentModel;

namespace NfeUtils.NfeClasses
{
    [System.Serializable()]
    [DesignerCategory("code")]
    //[XmlType(AnonymousType = true, Namespace = "http://www.w3.org/2000/09/xmldsig#")]
    public class SignatureSignedInfoReferenceDigestMethod
    {

        [XmlAttribute]
        public string Algorithm { get; set; }
    }


}

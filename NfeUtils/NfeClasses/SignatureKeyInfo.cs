﻿using System.ComponentModel;
using System;

namespace NfeUtils.NfeClasses
{
    [Serializable]
    [DesignerCategory("code")]
    public class SignatureKeyInfo
    {
        public SignatureKeyInfoX509Data X509Data { get; set; }
    }


}

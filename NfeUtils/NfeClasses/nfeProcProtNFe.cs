﻿using System;
using System.ComponentModel;
using System.Xml.Serialization;

namespace NfeUtils.NfeClasses
{
    [Serializable]
    [DesignerCategory("code")]
    public class nfeProcProtNFe
    {

        public nfeProcProtNFeInfProt infProt { get; set; }

        [XmlAttribute]
        public string versao { get; set; }
    }


}

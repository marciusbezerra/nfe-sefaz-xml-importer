﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using System.ComponentModel;

namespace NfeUtils.NfeClasses
{
    [Serializable()]
    [DesignerCategory("code")]
    //[XmlType(AnonymousType = true, Namespace = "http://www.w3.org/2000/09/xmldsig#")]
    //[XmlRoot(Namespace = "http://www.w3.org/2000/09/xmldsig#", IsNullable = false)]
    public class Signature
    {

        public SignatureSignedInfo SignedInfo { get; set; }
        public string SignatureValue { get; set; }
        public SignatureKeyInfo KeyInfo { get; set; }
    }
}

﻿using System.Xml.Serialization;
using System.ComponentModel;
using System;

namespace NfeUtils.NfeClasses
{
    [Serializable]
    [DesignerCategory("code")]
    //[XmlType(AnonymousType = true, Namespace = "http://www.w3.org/2000/09/xmldsig#")]
    public class SignatureKeyInfoX509Data
    {

        public string X509Certificate { get; set; }
    }


}

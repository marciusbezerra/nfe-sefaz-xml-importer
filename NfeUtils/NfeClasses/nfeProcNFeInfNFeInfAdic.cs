﻿using System.ComponentModel;
using System.Xml.Serialization;
using System;

namespace NfeUtils.NfeClasses
{
    [Serializable]
    [DesignerCategory("code")]
    //[XmlType(AnonymousType = true, Namespace = "http://www.portalfiscal.inf.br/nfe")]
    public partial class nfeProcNFeInfNFeInfAdic
    {
        public string infCpl { get; set; }

        [System.Xml.Serialization.XmlElementAttribute("obsCont")]
        public nfeProcNFeInfNFeInfAdicObsCont[] obsCont { get; set; }
    }


}

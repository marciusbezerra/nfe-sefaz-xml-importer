﻿namespace NfeUtils.NfeClasses
{
    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    ////[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.portalfiscal.inf.br/nfe")]
    public partial class nfeProcNFeInfNFeDetImpostoPIS
    {
        public nfeProcNFeInfNFeDetImpostoPISPIS PISAliq { get; set; }
        public nfeProcNFeInfNFeDetImpostoPISPIS PISNT { get; set; }
    }


}

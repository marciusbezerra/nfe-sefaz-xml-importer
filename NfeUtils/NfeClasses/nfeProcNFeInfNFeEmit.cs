﻿namespace NfeUtils.NfeClasses
{
    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    ////[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.portalfiscal.inf.br/nfe")]
    public partial class nfeProcNFeInfNFeEmit
    {
        public string CNPJ { get; set; }
        public string xNome { get; set; }
        public nfeProcNFeInfNFeEmitEnderEmit enderEmit { get; set; }
        public string IE { get; set; }

        public bool ShouldSerializeIEST() => !string.IsNullOrWhiteSpace(IEST);
        public string IEST { get; set; }

        public string CRT { get; set; }
    }


}

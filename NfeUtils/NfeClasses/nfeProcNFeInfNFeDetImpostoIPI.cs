﻿namespace NfeUtils.NfeClasses
{
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    ////[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.portalfiscal.inf.br/nfe")]
    public class nfeProcNFeInfNFeDetImpostoIPI
    {
        public bool ShouldSerializeclEnq() => !string.IsNullOrWhiteSpace(clEnq);

        public string clEnq { get; set; }

        public bool ShouldSerializeCNPJProd() => !string.IsNullOrWhiteSpace(CNPJProd);

        public string CNPJProd { get; set; }

        public bool ShouldSerializecSelo() => !string.IsNullOrWhiteSpace(cSelo);

        public string cSelo { get; set; }

        public bool ShouldSerializeqSelo() => !string.IsNullOrWhiteSpace(qSelo);

        public string qSelo { get; set; }

        public bool ShouldSerializecEnq() => !string.IsNullOrWhiteSpace(cEnq);

        public string cEnq { get; set; }

        public nfeProcNFeInfNFeDetImpostoIPITrib IPITrib { get; set; }
    }
}
﻿using System;
using System.ComponentModel;
using System.Xml.Serialization;

namespace NfeUtils.NfeClasses
{
    [Serializable]
    [DesignerCategory("code")]

    public class nfeProcProtNFeInfProt
    {
        [XmlAttribute]
        public string Id { get; set; }

        public byte tpAmb { get; set; }

        public string verAplic { get; set; }

        [XmlElement(DataType = "integer")]
        public string chNFe { get; set; }

        public string dhRecbto { get; set; }

        public ulong nProt { get; set; }

        public string digVal { get; set; }

        public byte cStat { get; set; }

        public string xMotivo { get; set; }
    }


}

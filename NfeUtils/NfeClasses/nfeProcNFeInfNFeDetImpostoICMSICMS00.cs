﻿using System;
using System.ComponentModel;
using System.Xml.Serialization;

namespace NfeUtils.NfeClasses
{
    [Serializable]
    [DesignerCategory("code")]
    //[XmlType(AnonymousType = true, Namespace = "http://www.portalfiscal.inf.br/nfe")]
    public partial class nfeProcNFeInfNFeDetImpostoICMSICMS
    {

        public string orig { get; set; }
        public string CST { get; set; }
        public string modBC { get; set; }
        public string pRedBC { get; set; }
        public string vBC { get; set; }
        public string vBCSTRet { get; set; }
        public string pST { get; set; }
        public string vICMSSTRet { get; set; }
        public string vBCFCPSTRet { get; set; }
        public string pFCPSTRet { get; set; }
        public string vFCPSTRet { get; set; }
        public string pRedBCEfet { get; set; }
        public string vBCEfet { get; set; }
        public string pICMSEfet { get; set; }
        public string vICMSEfet { get; set; }
        public string pICMS { get; set; }
        public string vICMS { get; set; }
    }


}

﻿namespace NfeUtils.NfeClasses
{
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    ////[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.portalfiscal.inf.br/nfe")]
    public partial class nfeProcNFeInfNFeDetImpostoICMSUFDest
    {
        public string vBCUFDest { get; set; }
        public string pFCPUFDest { get; set; }
        public string pICMSUFDest { get; set; }
        public string pICMSInter { get; set; }

        public bool ShouldSerializepICMSInterPart()
        {
            return !string.IsNullOrWhiteSpace(pICMSInterPart) && (pICMSInterPart != "0.00") && (!pICMSInterPart.Contains("-"));
        }
        public string pICMSInterPart { get; set; }

        public string vFCPUFDest { get; set; }
        public string vICMSUFDest { get; set; }
        public string vICMSUFRemet { get; set; }
    }


}

﻿using NfeUtils.NfeClassess;
using System;
using System.ComponentModel;
using System.Xml.Serialization;

namespace NfeUtils.NfeClasses
{
    [Serializable]
    [DesignerCategory("code")]
    //[XmlType(AnonymousType = true, Namespace = "http://www.portalfiscal.inf.br/nfe")]
    //[XmlRoot(Namespace = "http://www.portalfiscal.inf.br/nfe", IsNullable = true)]
    public class nfeProc
    {

        public nfeProcNFe NFe { get; set; }

        public nfeProcProtNFe protNFe { get; set; }

        [XmlAttribute()]
        public string versao { get; set; }
    }


}

﻿namespace NfeUtils.NfeClasses
{
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    ////[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.portalfiscal.inf.br/nfe")]
    public class nfeProcNFeInfNFeDetImpostoIPITrib
    {
        public string CNPJProd { get; set; }

        public string CST { get; set; }

        public string vBC { get; set; }

        public string pIPI { get; set; }

        public string vIPI { get; set; }
        //public string qUnid { get; set; }
    }
}
﻿using System.Xml.Serialization;
using System;
using System.ComponentModel;

namespace NfeUtils.NfeClasses
{
    [Serializable]
    [DesignerCategory("code")]
    ////[XmlType(AnonymousType = true, Namespace = "http://www.portalfiscal.inf.br/nfe")]
    public class nfeProcNFeInfNFe
    {
        [XmlAttribute]
        public string Id { get; set; }

        [XmlAttribute]
        public string versao { get; set; }

        public nfeProcNFeInfNFeIde ide { get; set; }
        public nfeProcNFeInfNFeEmit emit { get; set; }
        public nfeProcNFeInfNFeDest dest { get; set; }

        [XmlElement("det")]
        public nfeProcNFeInfNFeDet[] det { get; set; }

        public nfeProcNFeInfNFeTotal total { get; set; }
        public nfeProcNFeInfNFeTransp transp { get; set; }
        public nfeProcNFeInfNFepag pag { get; set; }
        public nfeProcNFeInfNFeInfAdic infAdic { get; set; }
    }


}

﻿using System;
using System.ComponentModel;
using System.Xml.Serialization;

namespace NfeUtils.NfeClasses
{
    [Serializable]
    [DesignerCategory("code")]
    ////[XmlType(AnonymousType = true, Namespace = "http://www.portalfiscal.inf.br/nfe")]
    public partial class nfeProcNFeInfNFeDetProd
    {
        public string cProd { get; set; }
        public string cEAN { get; set; }
        public string xProd { get; set; }
        public string NCM { get; set; }

        public bool ShouldSerializeCEST() => !string.IsNullOrWhiteSpace(CEST);
        public string CEST { get; set; }

        public string CFOP { get; set; }
        public string uCom { get; set; }
        public string qCom { get; set; }
        public string vUnCom { get; set; }
        public string vProd { get; set; }
        public string cEANTrib { get; set; }
        public string uTrib { get; set; }
        public string qTrib { get; set; }
        public string vUnTrib { get; set; }

        public bool ShouldSerializevDesc() => !string.IsNullOrWhiteSpace(vDesc) && (vDesc != "0.00");
        public string vDesc { get; set; }

        public string indTot { get; set; }
    }


}

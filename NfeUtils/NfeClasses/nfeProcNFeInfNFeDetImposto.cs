﻿namespace NfeUtils.NfeClasses
{
    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    ////[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.portalfiscal.inf.br/nfe")]
    public partial class nfeProcNFeInfNFeDetImposto
    {

        public string vTotTrib { get; set; }

        public nfeProcNFeInfNFeDetImpostoICMS ICMS { get; set; }

        public bool ShouldSerializeIPI()
        {
            return !string.IsNullOrWhiteSpace(IPI.IPITrib.vIPI) && IPI.IPITrib.vIPI != "0.00";
        }


        public nfeProcNFeInfNFeDetImpostoIPI IPI { get; set; }

        public nfeProcNFeInfNFeDetImpostoPIS PIS { get; set; }

        public nfeProcNFeInfNFeDetImpostoCOFINS COFINS { get; set; }

        public bool ShouldSerializeICMSUFDest()
        {
            return ((ICMSUFDest?.pFCPUFDest != null) && (ICMSUFDest.pFCPUFDest != "0.00") && (!ICMSUFDest.pFCPUFDest.Contains("-"))) ||
                ((ICMSUFDest?.pICMSUFDest != null) && (ICMSUFDest.pICMSUFDest != "0.00") && (!ICMSUFDest.pICMSUFDest.Contains("-"))) ||
                ((ICMSUFDest?.pICMSInter != null) && (ICMSUFDest.pICMSInter != "0.00") && (!ICMSUFDest.pICMSInter.Contains("-"))) ||
                ((ICMSUFDest?.pICMSInterPart != null) && (ICMSUFDest.pICMSInterPart != "0.00") && (!ICMSUFDest.pICMSInterPart.Contains("-"))) ||
                ((ICMSUFDest?.vFCPUFDest != null) && (ICMSUFDest.vFCPUFDest != "0.00") && (!ICMSUFDest.vFCPUFDest.Contains("-"))) ||
                ((ICMSUFDest?.vICMSUFDest != null) && (ICMSUFDest.vICMSUFDest != "0.00") && (!ICMSUFDest.vICMSUFDest.Contains("-"))) ||
                ((ICMSUFDest?.vICMSUFRemet != null) && (ICMSUFDest.vICMSUFRemet != "0.00") && (!ICMSUFDest.vICMSUFRemet.Contains("-")));
        }

        public nfeProcNFeInfNFeDetImpostoICMSUFDest ICMSUFDest { get; set; }
    }


}

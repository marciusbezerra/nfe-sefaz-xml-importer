﻿using System.Xml.Serialization;
using System.ComponentModel;
using System;

namespace NfeUtils.NfeClasses
{
    [Serializable]
    [DesignerCategory("code")]
    //[XmlType(AnonymousType = true, Namespace = "http://www.w3.org/2000/09/xmldsig#")]
    public class SignatureSignedInfoReference
    {

        [XmlArrayItem("Transform", IsNullable = false)]
        public SignatureSignedInfoReferenceTransform[] Transforms { get; set; }

        public SignatureSignedInfoReferenceDigestMethod DigestMethod { get; set; }

        public string DigestValue { get; set; }

        [XmlAttribute]
        public string URI { get; set; }
    }


}

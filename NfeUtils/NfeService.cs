﻿using HtmlAgilityPack;
using NfeUtils.NfeClasses;
using NfeUtils.NfeClassess;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml;
using System.Xml.Serialization;

namespace NfeUtils
{
    public static class NfeService
    {
        public static nfeProc GetNfeProc(HtmlDocument html)
        {
            string numeroNf = GetInnerText(html, "//*[@id=\"lblChaveAcesso\"]");
            ExtractInfoDoNumeroDaNf(numeroNf, out string cUf, out string cNf, out string mod, out string indPag, out string cDV);

            string enderecoCompletoEmit = GetInnerText(html, "//*[@id=\"Emitente\"]/fieldset/table//tr[2]/td[2]/span");
            ExtractEndereco(enderecoCompletoEmit, out string xLgrEmit, out string nroEmit, out string xCplEmit);

            string enderecoCompletoDest = GetInnerText(html, "//*[@id='DestRem']/fieldset/table//tr[2]/td[2]/span");
            ExtractEndereco(enderecoCompletoDest, out string xLgrDest, out string nroDest, out string xCplDest);

            string xmlVersao = string.Join("", GetInnerText(html, "//*[@id=\"form1\"]/div[5]/table//tr/td[3]/span[2]")?.Where(c => char.IsDigit(c) || char.IsPunctuation(c)));

            string docDest = GetInnerLong(html, "//*[@id='DestRem']/fieldset/table//tr[2]/td[1]/span");

            if (xmlVersao != "4.00") throw new Exception("Homologado apenas para versão 4.00");

            nfeProc nfeProc = new nfeProc
            {
                versao = xmlVersao,
                NFe = new nfeProcNFe
                {
                    infNFe = new nfeProcNFeInfNFe
                    {
                        Id = $"NFe{GetInnerLong(html, "//*[@id=\"lblChaveAcesso\"]")}",
                        versao = xmlVersao,
                        ide = new nfeProcNFeInfNFeIde
                        {
                            cUF = cUf,
                            cNF = cNf,
                            natOp = GetInnerText(html, "//*[@id=\"NFe\"]/fieldset[4]/table//tr[2]/td[1]/span"),
                            indPag = GetInnerTextCode(html, "//*[@id=\"NFe\"]/fieldset[4]/table//tr[2]/td[3]/span"),
                            mod = mod,
                            serie = GetInnerLong(html, "//*[@id=\"NFe\"]/fieldset[1]/table//tr/td[2]/span"),
                            nNF = GetInnerLong(html, "//*[@id=\"NFe\"]/fieldset[1]/table//tr/td[3]/span"),
                            dhEmi = GetInnerDateTime(html, "//*[@id=\"NFe\"]/fieldset[1]/table//tr/td[4]/span"),
                            dhSaiEnt = GetInnerDateTime(html, "//*[@id=\"NFe\"]/fieldset[1]/table//tr/td[5]/span"),
                            tpNF = GetInnerTextCode(html, "//*[@id=\"NFe\"]/fieldset[4]/table//tr[2]/td[2]/span"),
                            idDest = GetInnerTextCode(html, "//*[@id=\"NFe\"]/fieldset[3]/table//tr[2]/td[1]/span"),
                            cMunFG = GetInnerTextCode(html, "//*[@id=\"Emitente\"]/fieldset/table//tr[4]/td[1]/span"), // 3522505
                            tpImp = GetInnerTextCode(html, "//*[@id=\"Inf\"]/fieldset/table//tr/td/span"), // 1
                            tpEmis = GetInnerTextCode(html, "//*[@id=\"NFe\"]/fieldset[4]/table//tr[1]/td[3]/span"), // 1
                            cDV = cDV,
                            tpAmb = "1",
                            finNFe = GetInnerTextCode(html, "//*[@id=\"NFe\"]/fieldset[4]/table//tr[1]/td[4]/span"), // 1
                            indFinal = GetInnerTextCode(html, "//*[@id=\"NFe\"]/fieldset[3]/table//tr[2]/td[2]/span"), // 1,
                            indPres = GetInnerTextCode(html, "//*[@id=\"NFe\"]/fieldset[3]/table//tr[2]/td[3]/span"), // 2,
                            procEmi = GetInnerTextCode(html, "//*[@id=\"NFe\"]/fieldset[4]/table//tr[1]/td[1]/span"), // 0,
                            verProc = GetInnerText(html, "//*[@id=\"NFe\"]/fieldset[4]/table//tr[1]/td[2]/span"), // "ELDOC",
                            NFref = new nfeProcNFeInfNFeIdeNFref
                            {
                                refNFe = GetInnerLong(html, "//*[@id=\"Inf\"]/fieldset/fieldset[3]/fieldset/table//tr[1]/td[1]/span") //"35180500776574000741550030300066951126856563"
                            }
                        },
                        emit = new nfeProcNFeInfNFeEmit
                        {
                            CNPJ = GetInnerLong(html, "//*[@id=\"NFe\"]/fieldset[2]/table//tr/td[1]/span"), // 00776574002280,
                            xNome = GetInnerText(html, "//*[@id=\"NFe\"]/fieldset[2]/table//tr/td[2]/span"), // "B2W Companhia Digital",
                            enderEmit = new nfeProcNFeInfNFeEmitEnderEmit
                            {
                                xLgr = xLgrEmit,
                                nro = nroEmit,
                                xCpl = xCplEmit,
                                xBairro = GetInnerText(html, "//*[@id=\"Emitente\"]/fieldset/table//tr[3]/td[1]/span"),
                                cMun = GetInnerTextCode(html, "//*[@id=\"Emitente\"]/fieldset/table//tr[4]/td[1]/span"),
                                xMun = GetInnerTextValue(html, "//*[@id=\"Emitente\"]/fieldset/table//tr[4]/td[1]/span"),
                                UF = GetInnerText(html, "//*[@id=\"Emitente\"]/fieldset/table//tr[5]/td[1]/span"),
                                CEP = GetInnerLong(html, "//*[@id=\"Emitente\"]/fieldset/table//tr[3]/td[2]/span"),
                                cPais = GetInnerTextCode(html, "//*[@id=\"Emitente\"]/fieldset/table//tr[5]/td[2]/span"),
                                xPais = GetInnerTextValue(html, "//*[@id=\"Emitente\"]/fieldset/table//tr[5]/td[2]/span")
                            },
                            IE = GetInnerText(html, "//*[@id=\"Emitente\"]/fieldset/table//tr[6]/td[1]/span"),
                            IEST = GetInnerText(html, "//*[@id=\"Emitente\"]/fieldset/table//tr[6]/td[2]/span"),
                            CRT = GetInnerTextCode(html, "//*[@id=\"Emitente\"]/fieldset/table//tr[8]/td[2]/span")
                        },
                        dest = new nfeProcNFeInfNFeDest
                        {
                            CPF = docDest.Length <= 11 ? docDest : null,
                            CNPJ = docDest.Length > 11 ? docDest : null,
                            xNome = GetInnerText(html, "//*[@id='DestRem']/fieldset/table//tr[1]/td/span"),
                            enderDest = new nfeProcNFeInfNFeDestEnderDest
                            {
                                xLgr = xLgrDest,
                                nro = nroDest,
                                xCpl = xCplDest,
                                xBairro = GetInnerText(html, "//*[@id='DestRem']/fieldset/table//tr[3]/td[1]/span"),
                                cMun = GetInnerTextCode(html, "//*[@id='DestRem']/fieldset/table//tr[4]/td[1]/span"),
                                xMun = GetInnerTextValue(html, "//*[@id='DestRem']/fieldset/table//tr[4]/td[1]/span"),
                                UF = GetInnerText(html, "//*[@id='DestRem']/fieldset/table//tr[5]/td[1]/span"),
                                CEP = GetInnerLong(html, "//*[@id='DestRem']/fieldset/table//tr[3]/td[2]/span"),
                                cPais = GetInnerTextCode(html, "//*[@id='DestRem']/fieldset/table//tr[5]/td[2]/span"),
                                xPais = GetInnerTextValue(html, "//*[@id='DestRem']/fieldset/table//tr[5]/td[2]/span"),
                                fone = GetInnerLong(html, "//*[@id='DestRem']/fieldset/table//tr[4]/td[2]/span")
                            },
                            IE = GetInnerText(html, "//*[@id=\"NFe\"]/fieldset[3]/table//tr[1]/td[3]/span", null),
                            indIEDest = int.Parse(GetInnerTextCode(html, "//*[@id=\"DestRem\"]/fieldset/table//tr[6]/td[1]/span")).ToString()
                        },
                        total = new nfeProcNFeInfNFeTotal
                        {
                            ICMSTot = new nfeProcNFeInfNFeTotalICMSTot
                            {
                                vBC = GetInnerDecimal(html, "//*[@id=\"Totais\"]/fieldset/fieldset/table//tr[1]/td[1]/span", 2),
                                vICMS = GetInnerDecimal(html, "//*[@id=\"Totais\"]/fieldset/fieldset/table//tr[1]/td[2]/span", 2),
                                vICMSDeson = GetInnerDecimal(html, "//*[@id=\"Totais\"]/fieldset/fieldset/table//tr[1]/td[3]/span", 2),
                                vFCPUFDest = GetInnerDecimal(html, "//*[@id=\"Totais\"]/fieldset/fieldset/table//tr[1]/td[4]/span", 2),
                                vICMSUFDest = GetInnerDecimal(html, "//*[@id=\"Totais\"]/fieldset/fieldset/table//tr[2]/td[2]/span", 2),
                                vICMSUFRemet = GetInnerDecimal(html, "//*[@id=\"Totais\"]/fieldset/fieldset/table//tr[2]/td[3]/span", 2),
                                vFCP = GetInnerDecimal(html, "//*[@id=\"Totais\"]/fieldset/fieldset/table//tr[2]/td[1]/span", 2, "0.00"),
                                vBCST = GetInnerDecimal(html, "//*[@id=\"Totais\"]/fieldset/fieldset/table//tr[2]/td[4]/span", 2),
                                vST = GetInnerDecimal(html, "//*[@id=\"Totais\"]/fieldset/fieldset/table//tr[2]/td[4]/span", 2),
                                vFCPST = GetInnerDecimal(html, "//*[@id=\"Totais\"]/fieldset/fieldset/table//tr[3]/td[2]/span", 2, "0.00"),
                                vFCPSTRet = GetInnerDecimal(html, "//*[@id=\"Totais\"]/fieldset/fieldset/table//tr[3]/td[3]/span", 2, "0.00"),
                                vProd = GetInnerDecimal(html, "//*[@id=\"Totais\"]/fieldset/fieldset/table//tr[3]/td[4]/span", 2),
                                vFrete = GetInnerDecimal(html, "//*[@id=\"Totais\"]/fieldset/fieldset/table//tr[4]/td[1]/span", 2),
                                vSeg = GetInnerDecimal(html, "//*[@id=\"Totais\"]/fieldset/fieldset/table//tr[4]/td[2]/span", 2),
                                vDesc = GetInnerDecimal(html, "//*[@id=\"Totais\"]/fieldset/fieldset/table//tr[4]/td[3]/span", 2),
                                vII = GetInnerDecimal(html, "//*[@id=\"Totais\"]/fieldset/fieldset/table//tr[4]/td[4]/span", 2),
                                vIPI = GetInnerDecimal(html, "//*[@id=\"Totais\"]/fieldset/fieldset/table//tr[5]/td[1]/span", 2),
                                vIPIDevol = GetInnerDecimal(html, "//*[@id=\"Totais\"]/fieldset/fieldset/table//tr[5]/td[2]/span", 2, "0.00"),
                                vPIS = GetInnerDecimal(html, "//*[@id=\"Totais\"]/fieldset/fieldset/table//tr[5]/td[3]/span", 2),
                                vCOFINS = GetInnerDecimal(html, "//*[@id=\"Totais\"]/fieldset/fieldset/table//tr[5]/td[4]/span", 2),
                                vOutro = GetInnerDecimal(html, "//*[@id=\"Totais\"]/fieldset/fieldset/table//tr[6]/td[1]/span", 2),
                                vNF = GetInnerDecimal(html, "//*[@id=\"Totais\"]/fieldset/fieldset/table//tr[6]/td[2]/span", 2),
                                vTotTrib = GetInnerDecimal(html, "//*[@id=\"Totais\"]/fieldset/fieldset/table//tr[6]/td[3]/span", 2)
                            }
                        },
                        pag = new nfeProcNFeInfNFepag
                        {
                            detPag = new nfeProcNFeInfNFepagdetPag
                            {
                                tPag = GetInnerTextCode(html, "//*[@id=\"Cobranca\"]/fieldset/fieldset/table//tr[2]/td[1]/span"), //Forma de pagamento
                                vPag = GetInnerDecimal(html, "//*[@id=\"Cobranca\"]/fieldset/fieldset/table//tr[2]/td[2]/span"), //Valor pago
                                card = new nfeProcNFeInfNFepagdetPagcard
                                {
                                    tpIntegra = GetInnerTextCode(html, "//*[@id=\"Cobranca\"]/fieldset/fieldset/table//tr[2]/td[3]/span", null), //Tipo de Integração do processo de pagamento
                                    CNPJ = GetInnerText(html, "//*[@id=\"Cobranca\"]/fieldset/fieldset/table//tr[2]/td[4]/span", null), //CNPJ da credenciadora de cartão de crédito/débito
                                    tBand = GetInnerTextCode(html, "//*[@id=\"Cobranca\"]/fieldset/fieldset/table//tr[2]/td[5]/span", null), //bandeira
                                    cAut = GetInnerText(html, "//*[@id=\"Cobranca\"]/fieldset/fieldset/table//tr[2]/td[6]/span", null), //Número de autorização da operação
                                },
                                vTroco = GetInnerDecimal(html, "//*[@id=\"Cobranca\"]/fieldset/fieldset/table//tr[4]/td/span", 2, null)  //troco
                            }
                        },
                        infAdic = new nfeProcNFeInfNFeInfAdic //TODO: CRIAR ARRAY!
                        {
                            infCpl = GetInnerText(html, "//*[@id=\"Inf\"]/fieldset/fieldset[1]/table//tr/td/span/div"),
                        }
                    },
                    Signature = new Signature
                    {
                        SignedInfo = new SignatureSignedInfo
                        {
                            CanonicalizationMethod = new SignatureSignedInfoCanonicalizationMethod
                            {
                                Algorithm = "http://www.w3.org/TR/2001/REC-xml-c14n-20010315",
                            },
                            SignatureMethod = new SignatureSignedInfoSignatureMethod
                            {
                                Algorithm = "http://www.w3.org/2000/09/xmldsig#rsa-sha1"
                            },
                            Reference = new SignatureSignedInfoReference
                            {
                                URI = "#NFe35180819364681000103550010001110151003154977",
                                Transforms = new SignatureSignedInfoReferenceTransform[]
                                {
                                    new SignatureSignedInfoReferenceTransform
                                    {
                                        Algorithm = "http://www.w3.org/2000/09/xmldsig#enveloped-signature"
                                    },
                                    new SignatureSignedInfoReferenceTransform
                                    {
                                        Algorithm = "http://www.w3.org/TR/2001/REC-xml-c14n-20010315"
                                    }
                                },
                                DigestMethod = new SignatureSignedInfoReferenceDigestMethod
                                {
                                    Algorithm = "http://www.w3.org/2000/09/xmldsig#sha1"
                                },
                                DigestValue = "XTQMiOWAuD75punw285XCc4tTbw="
                            }
                        },
                        SignatureValue = "Ncxf4NsgWlYMNXu6QSVtxmy38jV2z7Xe24V0icSUomOUfHStwXmwubj1VsuAWR+ZLI1b8rXh5VARjacRijAWPJykln1kNmTWZzNCEIjHJS++r/AT3Ku7mw9V+up5B7Cj0V74CFG6GjEMO05L3mip0RVloErNHkE7cvcQKrUv96s8DSUn4R1YL2QKkWTFqFv6PDpjDp78EdvfZQN+yETW2lydkTYqfNRbj9oAhseD9lLeg/TCryOWbn0jK4pP2Q2INqOhqwpZ1xJBOGIr5YHJCUFKl+KAF2NX9XURLjHtCTc8L6WLn+BYUqDTwPPbjfa5tRnS0cQ7WNOAUjKz6HCWNQ==",
                        KeyInfo = new SignatureKeyInfo
                        {
                            X509Data = new SignatureKeyInfoX509Data
                            {
                                X509Certificate = "MIIIIjCCBgqgAwIBAgIIFumzaB8gZFUwDQYJKoZIhvcNAQELBQAwcTELMAkGA1UEBhMCQlIxEzARBgNVBAoTCklDUC1CcmFzaWwxNjA0BgNVBAsTLVNlY3JldGFyaWEgZGEgUmVjZWl0YSBGZWRlcmFsIGRvIEJyYXNpbCAtIFJGQjEVMBMGA1UEAxMMQUMgVkFMSUQgUkZCMB4XDTE3MTEyMzE5MzMxOFoXDTE4MTEyMzE5MzMxOFowgfQxCzAJBgNVBAYTAkJSMQswCQYDVQQIEwJTUDEQMA4GA1UEBxMHQ0FKQU1BUjETMBEGA1UEChMKSUNQLUJyYXNpbDE2MDQGA1UECxMtU2VjcmV0YXJpYSBkYSBSZWNlaXRhIEZlZGVyYWwgZG8gQnJhc2lsIC0gUkZCMRYwFAYDVQQLEw1SRkIgZS1DTlBKIEExMRYwFAYDVQQLEw1BUiBJTkZPUk1CQU5LMUkwRwYDVQQDE0BNQVJLRVRQTEFDRSBDT01FUkNJTyBERSBQUk9EVVRPUyBBTElNRU5USUNJT1MgRSBSOjE5MzY0NjgxMDAwMTAzMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA90twj2f0h1ACSxyiEsxebXOePMBllTyJUnerNXh8IDkrCxjGSvwJOH/hBFivUCRYhxl4fNU8AnoQDieGerKzrwbH1BRteNPCrsHgG3N77Ze+lTPqa3HIm6h+zvXj0IuHpMEb2Za0wchXCcfze/32Ht/POSHjlNudTqV27w4y0XXHyQUL3b5hd1+1lI+QWHldE+EJ0zGym1e2hv8C/Pq51RRH3uEKGsuth3qyT5z16yAs5igVr4PTNmEiX//P6NZwUIjnl2wyzFj53hDfp7PelZ1BJr3pmk0lOYrYu52ztHt9VYe2KKspAWL0rW6ZBYac7hiMH23qYZO2kstGysvYCQIDAQABo4IDODCCAzQwgZoGCCsGAQUFBwEBBIGNMIGKMFUGCCsGAQUFBzAChklodHRwOi8vaWNwLWJyYXNpbC52YWxpZGNlcnRpZmljYWRvcmEuY29tLmJyL2FjLXZhbGlkcmZiL2FjLXZhbGlkcmZidjIucDdiMDEGCCsGAQUFBzABhiVodHRwOi8vb2NzcC52YWxpZGNlcnRpZmljYWRvcmEuY29tLmJyMAkGA1UdEwQCMAAwHwYDVR0jBBgwFoAUR7kIWdhC9pL893wVfCaASkWRfp8wbgYDVR0gBGcwZTBjBgZgTAECASUwWTBXBggrBgEFBQcCARZLaHR0cDovL2ljcC1icmFzaWwudmFsaWRjZXJ0aWZpY2Fkb3JhLmNvbS5ici9hYy12YWxpZHJmYi9kcGMtYWMtdmFsaWRyZmIucGRmMIIBAQYDVR0fBIH5MIH2MFOgUaBPhk1odHRwOi8vaWNwLWJyYXNpbC52YWxpZGNlcnRpZmljYWRvcmEuY29tLmJyL2FjLXZhbGlkcmZiL2xjci1hYy12YWxpZHJmYnYyLmNybDBUoFKgUIZOaHR0cDovL2ljcC1icmFzaWwyLnZhbGlkY2VydGlmaWNhZG9yYS5jb20uYnIvYWMtdmFsaWRyZmIvbGNyLWFjLXZhbGlkcmZidjIuY3JsMEmgR6BFhkNodHRwOi8vcmVwb3NpdG9yaW8uaWNwYnJhc2lsLmdvdi5ici9sY3IvVkFMSUQvbGNyLWFjLXZhbGlkcmZidjIuY3JsMA4GA1UdDwEB/wQEAwIF4DAdBgNVHSUEFjAUBggrBgEFBQcDAgYIKwYBBQUHAwQwgcQGA1UdEQSBvDCBuYEeY2ludGhpYS5kYXJjQGhvbWVyZWZpbGwuY29tLmJyoD0GBWBMAQMEoDQEMjIwMDQxOTkwMzg3MTcyMDU4NTkyMDc4NDg5Mzc0MjAwMDAwMDM2MzU4NTE2M1NTUFNQoCQGBWBMAQMCoBsEGUdVSUxIRVJNRSBBRVJFIERPUyBTQU5UT1OgGQYFYEwBAwOgEAQOMTkzNjQ2ODEwMDAxMDOgFwYFYEwBAwegDgQMMDAwMDAwMDAwMDAwMA0GCSqGSIb3DQEBCwUAA4ICAQAgWSD0GvklDzEHE126tjfDTzyc9dfxvVt0s0sXo5q2LN2CKBO4xUgKQhbz4UmQxqBR9j2VmordTyfJryWHXavuYfD2xPsmCnqUBFTePomGJLJqlLmYD3nnrX/1d2yIK3Ktp4GdYfMOm6WW5q8YxjaDcXk4Eej/EbbRFnQT+WuD7C/evoVAdFJQJk5YFfk+zFkSX3VHwGvV7J3E36eRIngi9Cwcou2r1evlxTU0F9rIEL+1RGV5LcoeaGWeRPnocwLuCi2afJzV4XkouwbSiaF9xRU/NrT1PqojFjK/WyfpGPhcxoc0nEuieVxhbGBd9bUdzuaugQEs1X5CgKL+HMuJndPHM0ok8Hc+oCBQbNPkHO16TNqg+At2zDu1flrozDqeTzxyjxHcKuC2GmT4pw1fwRy5+TxRPJhDi+LMpK86ZcvYRgqUp0Z8sIHRU2aoUrwK0jYgbYH3gPQwQmc7y/o2EMlZF/d+hhnoOTxCtqWKGGJRloJD2BTHh1z31P7Ek6Ww07FrcGDv6er3jH66x/sep8fmnVIxhebeB+BBtuQs3ssrIvDO+W+Fw3XZ3EzEqg2hlBAw1+xRVkVt6HKHriT4DHT5p/4xt6A+kfiUxC29XYKuC1BEBFX8vEjv25SPzHQy/nFEPA0WAjtnFI5j1qu59XBrV+chINu6gMvLFgsCLA=="
                            }
                        }
                    }
                },
                protNFe = new nfeProcProtNFe
                {
                    versao = xmlVersao,
                    infProt = new nfeProcProtNFeInfProt
                    {
                        Id = "Id135180517580894",
                        tpAmb = 1,
                        verAplic = "SP_NFE_PL009_V4",
                        chNFe = GetInnerLong(html, "//*[@id=\"lblChaveAcesso\"]"), //"35180500776574002280550010010156101983838372",
                        dhRecbto = "2018-08-02T11:10:58-03:00", // GetInnerDateTime(html, "//*[@id=\"NFe\"]/fieldset[1]/table//tr/td[5]/span"),
                        nProt = 135180517580894, //*[@id="NFe"]/fieldset[5]/table/tbody/tr[2]/td[2]/span
                        digVal = "XTQMiOWAuD75punw285XCc4tTbw=", //*[@id="NFe"]/fieldset[4]/table/tbody/tr[2]/td[4]/span
                        cStat = 100,
                        xMotivo = "Autorizado o uso da NF-e"
                    }
                }
            };

            int i = 0;
            List<nfeProcNFeInfNFeDet> nfItens = new List<nfeProcNFeInfNFeDet>();

            while (true)
            {
                i += 2;
                string nItem = GetInnerLong(html, $"//*[@id='Prod']/fieldset/div/table[{i}]//tr/td[1]/span");

                if (nItem == null)
                {
                    break;
                }

                string legendIcms = GetInnerText(html, "//*[@id=\"Prod\"]/fieldset/div/table[3]//tr[1]/td[1]/table[2]//tr[7]/td/fieldset[2]/legend");
                bool temIcmsParaUfDestino = legendIcms?.ToLowerInvariant().Contains("icms para a uf de destino") == true;

                nfeProcNFeInfNFeDet newItem = new nfeProcNFeInfNFeDet
                {
                    nItem = nItem,
                    prod = new nfeProcNFeInfNFeDetProd
                    {
                        cProd = GetInnerLong(html, $"//*[@id=\"Prod\"]/fieldset/div/table[{i + 1}]//tr[1]/td[1]/table[1]//tr[1]/td[1]/span"),
                        cEAN = GetInnerLong(html, $"//*[@id=\"Prod\"]/fieldset/div/table[{i + 1}]//tr[1]/td[1]/table[2]//tr[2]/td[1]/span"),
                        xProd = GetInnerText(html, $"//*[@id=\"Prod\"]/fieldset/div/table[{i}]//tr[1]/td[2]/span"),
                        NCM = GetInnerLong(html, $"//*[@id=\"Prod\"]/fieldset/div/table[{i + 1}]//tr[1]/td[1]/table[1]//tr[1]/td[2]/span"),
                        CEST = GetInnerLong(html, $"//*[@id=\"Prod\"]/fieldset/div/table[{i + 1}]//tr[1]/td[1]/table[1]//tr[1]/td[3]/span"),
                        CFOP = GetInnerLong(html, $"//*[@id=\"Prod\"]/fieldset/div/table[{i + 1}]//tr[1]/td[1]/table[1]//tr[3]/td[2]/span"),
                        uCom = GetInnerText(html, $"//*[@id=\"Prod\"]/fieldset/div/table[{i + 1}]//tr[1]/td[1]/table[2]//tr[2]/td[2]/span"),
                        qCom = GetInnerDecimal(html, $"//*[@id=\"Prod\"]/fieldset/div/table[{i + 1}]//tr[1]/td[1]/table[2]//tr[2]/td[3]/span", 3),
                        vUnCom = GetInnerDecimal(html, $"//*[@id=\"Prod\"]/fieldset/div/table[{i + 1}]//tr[1]/td[1]/table[2]//tr[4]/td[1]/span", 6),
                        vProd = GetInnerDecimal(html, $"//*[@id=\"Prod\"]/fieldset/div/table[{i}]//tr[1]/td[5]/span", 2),
                        cEANTrib = GetInnerLong(html, $"//*[@id=\"Prod\"]/fieldset/div/table[{i + 1}]//tr[1]/td[1]/table[2]//tr[3]/td[1]/span"),
                        uTrib = GetInnerText(html, $"//*[@id=\"Prod\"]/fieldset/div/table[{i + 1}]//tr[1]/td[1]/table[2]//tr[3]/td[2]/span"),
                        qTrib = GetInnerDecimal(html, $"//*[@id=\"Prod\"]/fieldset/div/table[{i + 1}]//tr[1]/td[1]/table[2]//tr[3]/td[3]/span", 3),
                        vUnTrib = GetInnerDecimal(html, $"//*[@id=\"Prod\"]/fieldset/div/table[{i + 1}]//tr[1]/td[1]/table[2]//tr[4]/td[2]/span", 6),
                        vDesc = GetInnerDecimal(html, $"//*[@id=\"Prod\"]/fieldset/div/table[{i + 1}]//tr[1]/td[1]/table[1]//tr[4]/td[1]/span", 2),
                        indTot = GetInnerTextCode(html, $"//*[@id=\"Prod\"]/fieldset/div/table[{i + 1}]//tr[1]/td[1]/table[2]//tr[1]/td/span")
                    },
                    imposto = new nfeProcNFeInfNFeDetImposto
                    {
                        vTotTrib = GetInnerDecimal(html, $"//*[@id=\"Prod\"]/fieldset/div/table[{i + 1}]//tr/td/table[2]//tr[5]/td[3]/span", 2, null),
                        IPI = new nfeProcNFeInfNFeDetImpostoIPI
                        {
                            clEnq = GetLegendSpanValue(html.Text, "Imposto Sobre Produtos Industrializados", "Classe de Enquadramento", TipoExtracao.Text, int.Parse(nItem)),
                            CNPJProd = GetLegendSpanValue(html.Text, "Imposto Sobre Produtos Industrializados", "CNPJ do Produtor", TipoExtracao.Text, int.Parse(nItem)),
                            cSelo = GetLegendSpanValue(html.Text, "Imposto Sobre Produtos Industrializados", "Código do Selo", TipoExtracao.Text, int.Parse(nItem)),
                            qSelo = GetLegendSpanValue(html.Text, "Imposto Sobre Produtos Industrializados", "Qtd. Selo", TipoExtracao.Text, int.Parse(nItem)),
                            cEnq = GetLegendSpanValue(html.Text, "Imposto Sobre Produtos Industrializados", "Código de Enquadramento", TipoExtracao.Text, int.Parse(nItem)),
                            IPITrib = new nfeProcNFeInfNFeDetImpostoIPITrib
                            {
                                CST = GetLegendSpanValue(html.Text, "Imposto Sobre Produtos Industrializados", "CST", TipoExtracao.Code, int.Parse(nItem)),
                                //qUnid = GetLegendSpanValue(html.Text, "Imposto Sobre Produtos Industrializados", "Qtd Total Unidade Padrão", TipoExtracao.Decimal, int.Parse(nItem)),
                                vBC = GetLegendSpanValue(html.Text, "Imposto Sobre Produtos Industrializados", "Base de Cálculo", TipoExtracao.Decimal, int.Parse(nItem), "0"),
                                pIPI = GetLegendSpanValue(html.Text, "Imposto Sobre Produtos Industrializados", "Alíquota", TipoExtracao.Decimal, int.Parse(nItem), "0"),
                                vIPI = GetLegendSpanValue(html.Text, "Imposto Sobre Produtos Industrializados", "Valor IPI", TipoExtracao.Decimal, int.Parse(nItem), "0"),
                            }
                        },
                        ICMSUFDest = new nfeProcNFeInfNFeDetImpostoICMSUFDest
                        {
                            vBCUFDest = temIcmsParaUfDestino ? GetInnerDecimal(html, $"//*[@id=\"Prod\"]/fieldset/div/table[{i + 1}]//tr[1]/td[1]/table[2]//tr[7]/td/fieldset[2]/table//tr[1]/td[1]/span", 2) : null,
                            pFCPUFDest = temIcmsParaUfDestino ? GetInnerDecimal(html, $"//*[@id=\"Prod\"]/fieldset/div/table[{i + 1}]//tr[1]/td[1]/table[2]//tr[7]/td/fieldset[2]/table//tr[1]/td[3]/span", 2) : null,
                            pICMSUFDest = temIcmsParaUfDestino ? GetInnerDecimal(html, $"//*[@id=\"Prod\"]/fieldset/div/table[{i + 1}]//tr[1]/td[1]/table[2]//tr[7]/td/fieldset[2]/table//tr[2]/td[1]/span", 2) : null,
                            pICMSInter = temIcmsParaUfDestino ? GetInnerDecimal(html, $"//*[@id=\"Prod\"]/fieldset/div/table[{i + 1}]//tr[1]/td[1]/table[2]//tr[7]/td/fieldset[2]/table//tr[2]/td[2]/span", 2) : null,
                            pICMSInterPart = temIcmsParaUfDestino ? GetInnerDecimal(html, $"//*[@id=\"Prod\"]/fieldset/div/table[{i + 1}]//tr[1]/td[1]/table[2]//tr[7]/td/fieldset[2]/table//tr[2]/td[3]/span", 2) : null,
                            vFCPUFDest = temIcmsParaUfDestino ? GetInnerDecimal(html, $"//*[@id=\"Prod\"]/fieldset/div/table[{i + 1}]//tr[1]/td[1]/table[2]//tr[7]/td/fieldset[2]/table//tr[1]/td[2]/span", 2) : null,
                            vICMSUFDest = temIcmsParaUfDestino ? GetInnerDecimal(html, $"//*[@id=\"Prod\"]/fieldset/div/table[{i + 1}]//tr[1]/td[1]/table[2]//tr[7]/td/fieldset[2]/table//tr[3]/td[2]/span", 2) : null,
                            vICMSUFRemet = temIcmsParaUfDestino ? GetInnerDecimal(html, $"//*[@id=\"Prod\"]/fieldset/div/table[{i + 1}]//tr[1]/td[1]/table[2]//tr[7]/td/fieldset[2]/table//tr[3]/td[3]/span", 2) : null
                        },
                    },
                    infAdProd = GetInnerText(html, $"//*[@id=\"Prod\"]/fieldset/div/table[{i + 1}]//tr[1]/td[1]/table[2]//tr[14]/td/fieldset/table//tr[1]/td[1]/span")
                };


                newItem.imposto.ICMS = new nfeProcNFeInfNFeDetImpostoICMS();

                var CSTICMS = GetLegendSpanValue(html.Text, "ICMS Normal e ST", "Tributação do ICMS", TipoExtracao.Code, int.Parse(nItem));

                if (CSTICMS == "00")
                {
                    newItem.imposto.ICMS.ICMS00 = new nfeProcNFeInfNFeDetImpostoICMSICMS
                    {
                        orig = GetLegendSpanValue(html.Text, "ICMS Normal e ST", "Origem da Mercadoria", TipoExtracao.Code, int.Parse(nItem)),
                        CST = CSTICMS,
                        modBC = GetLegendSpanValue(html.Text, "ICMS Normal e ST", "Modalidade Definição da BC ICMS NORMAL", TipoExtracao.Code, int.Parse(nItem), "0"),
                        vBC = GetLegendSpanValue(html.Text, "ICMS Normal e ST", "Base de Cálculo do ICMS Normal", TipoExtracao.Decimal, int.Parse(nItem), "0"),
                        pICMS = GetLegendSpanValue(html.Text, "ICMS Normal e ST", "Alíquota do ICMS Normal", TipoExtracao.Decimal, int.Parse(nItem), "0"),
                        vICMS = GetLegendSpanValue(html.Text, "ICMS Normal e ST", "Valor do ICMS Normal", TipoExtracao.Decimal, int.Parse(nItem), "0"),
                    };
                }
                else if (CSTICMS == "20")
                {
                    newItem.imposto.ICMS.ICMS20 = new nfeProcNFeInfNFeDetImpostoICMSICMS
                    {
                        orig = GetLegendSpanValue(html.Text, "ICMS Normal e ST", "Origem da Mercadoria", TipoExtracao.Code, int.Parse(nItem)),
                        CST = CSTICMS,
                        modBC = GetLegendSpanValue(html.Text, "ICMS Normal e ST", "Modalidade Definição da BC do ICMS", TipoExtracao.Code, int.Parse(nItem), "0"),
                        pRedBC = GetLegendSpanValue(html.Text, "ICMS Normal e ST", "Percentual Redução de BC do ICMS Normal", TipoExtracao.Decimal, int.Parse(nItem), "0"),
                        vBC = GetLegendSpanValue(html.Text, "ICMS Normal e ST", "Base de Cálculo", TipoExtracao.Decimal, int.Parse(nItem), "0"),
                        pICMS = GetLegendSpanValue(html.Text, "ICMS Normal e ST", "Alíquota", TipoExtracao.Decimal, int.Parse(nItem), "0"),
                        vICMS = GetLegendSpanValue(html.Text, "ICMS Normal e ST", "Valor", TipoExtracao.Decimal, int.Parse(nItem), "0"),
                        //Valor ICMS Desonerado
                        //Motivo Desoneração ICMS
                        //Valor da Base de Cálculo do FCP
                        //Percentual do Fundo de Combate à Pobreza (FCP)
                        //Valor do Fundo de Combate à Pobreza (FCP)
                    };
                }
                else if (CSTICMS == "60")
                {
                    newItem.imposto.ICMS.ICMS60 = new nfeProcNFeInfNFeDetImpostoICMSICMS
                    {
                        orig = GetLegendSpanValue(html.Text, "ICMS Normal e ST", "Origem da Mercadoria", TipoExtracao.Code, int.Parse(nItem)),
                        CST = CSTICMS,
                        vBCSTRet = GetLegendSpanValue(html.Text, "ICMS Normal e ST", "Valor da BC do ICMS ST retido", TipoExtracao.Decimal, int.Parse(nItem)),
                        pST = GetLegendSpanValue(html.Text, "ICMS Normal e ST", "Alíquota suportada pelo Consumidor Final", TipoExtracao.Decimal, int.Parse(nItem)),
                        vICMSSTRet = GetLegendSpanValue(html.Text, "ICMS Normal e ST", "Valor do ICMS ST retido", TipoExtracao.Decimal, int.Parse(nItem), null),
                        vBCFCPSTRet = GetLegendSpanValue(html.Text, "ICMS Normal e ST", "Valor da Base de Cálculo do FCP retido anteriormente por ST", TipoExtracao.Decimal, int.Parse(nItem), null),
                        pFCPSTRet = GetLegendSpanValue(html.Text, "ICMS Normal e ST", "Percentual do FCP retido anteriormente por Substituição Tributária", TipoExtracao.Decimal, int.Parse(nItem), null),
                        vFCPSTRet = GetLegendSpanValue(html.Text, "ICMS Normal e ST", "Valor do FCP retido por Substituição Tributária", TipoExtracao.Decimal, int.Parse(nItem), null),
                        //pRedBCEfet = "0.00", //Campos novos!
                        //vBCEfet = "0.00", //Campos novos!
                        //pICMSEfet = "0.00", //Campos novos!
                        //vICMSEfet = "0.00", //Campos novos!
                    };
                }
                else if ((new[] { "40", "41", "50" }).Contains(CSTICMS))
                {
                    newItem.imposto.ICMS.ICMS40 = new nfeProcNFeInfNFeDetImpostoICMSICMS
                    {
                        orig = GetLegendSpanValue(html.Text, "ICMS Normal e ST", "Origem da Mercadoria", TipoExtracao.Code, int.Parse(nItem)),
                        CST = CSTICMS,
                        //vBCSTRet = GetLegendSpanValue(html.Text, "ICMS Normal e ST", "Valor da BC do ICMS ST retido", TipoExtracao.Decimal, int.Parse(nItem)),
                        //pST = GetLegendSpanValue(html.Text, "ICMS Normal e ST", "Alíquota suportada pelo Consumidor Final", TipoExtracao.Decimal, int.Parse(nItem)),
                        //vICMSSTRet = GetLegendSpanValue(html.Text, "ICMS Normal e ST", "Valor do ICMS ST retido", TipoExtracao.Decimal, int.Parse(nItem), "0.00"),
                        //vBCFCPSTRet = GetLegendSpanValue(html.Text, "ICMS Normal e ST", "Valor da Base de Cálculo do FCP retido anteriormente por ST", TipoExtracao.Decimal, int.Parse(nItem), "0.00"),
                        //pFCPSTRet = GetLegendSpanValue(html.Text, "ICMS Normal e ST", "Percentual do FCP retido anteriormente por Substituição Tributária", TipoExtracao.Decimal, int.Parse(nItem), "0.00"),
                        //vFCPSTRet = GetLegendSpanValue(html.Text, "ICMS Normal e ST", "Valor do FCP retido por Substituição Tributária", TipoExtracao.Decimal, int.Parse(nItem), "0.00"),
                        //pRedBCEfet = "0.00", //Campos novos!
                        //vBCEfet = "0.00", //Campos novos!
                        //pICMSEfet = "0.00", //Campos novos!
                        //vICMSEfet = "0.00", //Campos novos!
                    };
                }

                    newItem.imposto.PIS = new nfeProcNFeInfNFeDetImpostoPIS();

                var CSTPIS = GetLegendSpanValue(html.Text, "<legend class=\"toggle\">PIS</legend>", "CST", TipoExtracao.Code, int.Parse(nItem));

                if ((new[] { "01", "02" }).Contains(CSTPIS))
                {
                    newItem.imposto.PIS.PISNT = new nfeProcNFeInfNFeDetImpostoPISPIS
                    {
                        CST = CSTPIS,
                        vBC = GetLegendSpanValue(html.Text, "<legend class=\"toggle\">PIS</legend>", "Base de Cálculo", TipoExtracao.Decimal, int.Parse(nItem), "0"),
                        pPIS = GetLegendSpanValue(html.Text, "<legend class=\"toggle\">PIS</legend>", "Alíquota", TipoExtracao.Decimal, int.Parse(nItem), "0"),
                        vPIS = GetLegendSpanValue(html.Text, "<legend class=\"toggle\">PIS</legend>", "Valor", TipoExtracao.Decimal, int.Parse(nItem), "0")
                    };
                } else if ((new[] { "04", "06", "07", "08", "09" }).Contains(CSTPIS))
                {
                    newItem.imposto.PIS.PISNT = new nfeProcNFeInfNFeDetImpostoPISPIS
                    {
                        CST = CSTPIS,
                    };
                }

                newItem.imposto.COFINS = new nfeProcNFeInfNFeDetImpostoCOFINS();

                var CSTCOFINS = GetLegendSpanValue(html.Text, "<legend class=\"toggle\">COFINS</legend>", "CST", TipoExtracao.Code, int.Parse(nItem));

                if ((new[] { "01", "02" }).Contains(CSTCOFINS))
                {
                    newItem.imposto.COFINS.COFINSAliq = new nfeProcNFeInfNFeDetImpostoCOFINSCOFINS
                    {
                        CST = CSTCOFINS,
                        vBC = GetLegendSpanValue(html.Text, "<legend class=\"toggle\">COFINS</legend>", "Base de Cálculo", TipoExtracao.Decimal, int.Parse(nItem), "0"),
                        pCOFINS = GetLegendSpanValue(html.Text, "<legend class=\"toggle\">COFINS</legend>", "Alíquota", TipoExtracao.Decimal, int.Parse(nItem), "0"),
                        vCOFINS = GetLegendSpanValue(html.Text, "<legend class=\"toggle\">COFINS</legend>", "Valor", TipoExtracao.Decimal, int.Parse(nItem), "0")
                    };
                } else if ((new[] { "04", "06", "07", "08", "09" }).Contains(CSTCOFINS))
                {
                    newItem.imposto.COFINS.COFINSNT = new nfeProcNFeInfNFeDetImpostoCOFINSCOFINS
                    {
                        CST = CSTCOFINS,
                    };
                }

                nfItens.Add(newItem);

            }

            nfeProc.NFe.infNFe.det = nfItens.ToArray();

            i = 1;
            List<nfeProcNFeInfNFeInfAdicObsCont> obsCountList = new List<nfeProcNFeInfNFeInfAdicObsCont>();

            while (true)
            {
                i++;
                string xCampo = GetInnerText(html, $"//*[@id=\"Inf\"]/fieldset/fieldset[2]/table//tr[{i}]/td[1]/span");

                if (xCampo == null)
                {
                    break;
                }

                obsCountList.Add(new nfeProcNFeInfNFeInfAdicObsCont
                {
                    xCampo = xCampo,
                    xTexto = GetInnerText(html, $"//*[@id=\"Inf\"]/fieldset/fieldset[2]/table//tr[{i}]/td[2]/span")
                });
            }

            nfeProc.NFe.infNFe.infAdic.obsCont = obsCountList.ToArray();

            string cnpjTransportador = GetInnerText(html, "//*[@id=\"Transporte\"]/fieldset[2]/table//tr[1]/td[1]/span");

            i = string.IsNullOrWhiteSpace(cnpjTransportador) ? 2 : 3;

            nfeProc.NFe.infNFe.transp = new nfeProcNFeInfNFeTransp
            {
                modFrete = GetInnerTextCode(html, "//*[@id=\"Transporte\"]/fieldset[1]/table//tr/td/span"),
                vol = new nfeProcNFeInfNFeTranspVol
                {
                    qVol = GetInnerLong(html, $"//*[@id=\"Transporte\"]/fieldset[{i}]/table//tr[2]/td[1]/span"),
                    esp = GetInnerText(html, $"//*[@id=\"Transporte\"]/fieldset[{i}]/table//tr[2]/td[2]/span", "."),
                    pesoL = GetInnerDecimal(html, $"//*[@id=\"Transporte\"]/fieldset[{i}]/table//tr[3]/td[2]/span"),
                    pesoB = GetInnerDecimal(html, $"//*[@id=\"Transporte\"]/fieldset[{i}]/table//tr[3]/td[3]/span")
                }
            };

            return nfeProc;
        }

        public static void Save<T>(T file, string path)
        {
            var settings = new XmlWriterSettings
            {
                Indent = true,
                OmitXmlDeclaration = true,
                ConformanceLevel = ConformanceLevel.Auto
            };
            XmlSerializer serializer = new XmlSerializer(typeof(T));

            //var ns = new XmlSerializerNamespaces();
            //ns.Add("", "none");

            using (var writer = XmlWriter.Create(path, settings))
            {
                //serializer.Serialize(writer, file, ns);
                //serializer.Serialize(writer, file, null);
                serializer.Serialize(writer, file, new XmlSerializerNamespaces(new XmlQualifiedName[] { new XmlQualifiedName("", "") }));
            }
        }

        public static string ObjectToXml<T>(T obj)
        {
            var settings = new XmlWriterSettings
            {
                Indent = true,
                OmitXmlDeclaration = true,
                ConformanceLevel = ConformanceLevel.Auto
            };
            XmlSerializer serializer = new XmlSerializer(typeof(T));

            //var ns = new XmlSerializerNamespaces();
            //ns.Add("", "none");

            using (var stream = new StringWriter())
            using (var writer = XmlWriter.Create(stream, settings))
            {
                //serializer.Serialize(writer, obj, ns);
                //serializer.Serialize(writer, obj, null);
                serializer.Serialize(writer, obj, new XmlSerializerNamespaces(new XmlQualifiedName[] { new XmlQualifiedName("", "") }));
                return stream.ToString();
            }
        }

        public static void ExtractInfoDoNumeroDaNf(string numeroNf, out string cUf, out string cNf, out string mod, out string indPag, out string cDV)
        {
            string[] numeroParts = numeroNf.Split('-');
            cUf = numeroParts.FirstOrDefault();
            cNf = string.Join("", numeroParts[7]?.Where(char.IsDigit));
            cNf = cNf.Substring(1);
            mod = numeroParts[4];
            indPag = numeroParts.LastOrDefault();
            cDV = numeroParts.LastOrDefault();
        }

        public static void ExtractEndereco(string enderecoCompleto, out string xLgr, out string nro, out string xCpl)
        {
            string[] enderecoParts = enderecoCompleto.Split(new string[] { "\r\n" }, StringSplitOptions.None);
            xLgr = "";
            nro = "";
            xCpl = "";
            if (enderecoParts.Length > 0)
            {
                xLgr = enderecoParts.FirstOrDefault()?.Replace(",", "")?.Trim();
                if (enderecoParts.Length == 2)
                {
                    nro = enderecoParts[1]?.Trim();
                }
                else
                {
                    nro = enderecoParts[1]?.Trim();
                    xCpl = enderecoParts[2]?.Trim();
                }
            }
        }

        //public static string GetLegendSpanValue(string html, string legend, string[] possibleLabels, TipoExtracao tipo, int ocorrencia = 1, string valorPadrao = "")
        //{
        //    string result = "";
        //    foreach (string label in possibleLabels)
        //    {
        //        result = GetLegendSpanValue(html, legend, label, tipo, ocorrencia, valorPadrao);
        //        if (!string.IsNullOrWhiteSpace(result) && result != valorPadrao)
        //        {
        //            return result;
        //        }
        //    }
        //    return result;
        //}

        public static string GetLegendSpanValue(string html, string legend, string label, TipoExtracao tipo, int ocorrencia = 1, string valorPadrao = "")
        {
            int indLegend = html.IndexOf(legend, 0);
            for (int i = 1; i < ocorrencia; i++)
            {
                if (indLegend == -1)
                {
                    return null;
                }

                indLegend = html.IndexOf(legend, indLegend + 1);
            }
            if (indLegend >= 0)
            {
                int limite = html.IndexOf("</fieldset>", indLegend);
                int indLabel = html.IndexOf($"<label>{label}</label>", indLegend);
                if (indLabel == -1 || indLabel > limite)
                {
                    return valorPadrao;
                }

                int indSpanIni = html.IndexOf("<span>", indLabel) + "<span>".Length;
                int indSpanFim = html.IndexOf("</span>", indSpanIni);
                string value = html.Substring(indSpanIni, indSpanFim - indSpanIni);

                switch (tipo)
                {
                    case TipoExtracao.Code:
                        return value?.Split('-')?.FirstOrDefault()?.Trim();
                    case TipoExtracao.Decimal:
                        string result = string.Concat(value.Where(c => char.IsDigit(c) || char.IsPunctuation(c)));
                        if (string.IsNullOrWhiteSpace(result))
                        {
                            return valorPadrao;
                        }
                        result = Decimal.Parse(result).ToString($"F2");
                        result = result.Replace(".", "").Replace(",", ".");
                        return result;
                    default:
                        return value;
                }
            }
            return null;
        }

        public static string GetInnerText(HtmlDocument html, string xpath, string valorPadrao = "")
        {
            HtmlNode element = html.DocumentNode.SelectSingleNode(xpath);
            string result = element?.InnerText?.Trim();
            return result != "" ? result : valorPadrao;
        }

        public static string GetInnerDateTime(HtmlDocument html, string xpath)
        {
            HtmlNode element = html.DocumentNode.SelectSingleNode(xpath);
            string elementText = element?.InnerText;
            if (DateTime.TryParse(elementText, out DateTime dateTime))
            {
                string[] dateParts = elementText.Split('-');
                string difHors = "-03:00";
                if (dateParts.Length > 1)
                {
                    difHors = $"-{dateParts.LastOrDefault()}";
                }

                return dateTime.ToString("yyyy'-'MM'-'dd'T'HH':'mm':'ss") + difHors;
            }
            else
            {
                return "";
            }
        }

        public static string GetInnerValue(HtmlDocument html, string xpath)
        {
            HtmlNode element = html.DocumentNode.SelectSingleNode(xpath);
            return element?.GetAttributeValue("value", "");
        }

        public static string GetInnerLong(HtmlDocument html, string xpath)
        {
            HtmlNode element = html.DocumentNode.SelectSingleNode(xpath);
            string innerText = element?.InnerText;
            if (innerText != null)
            {
                return string.Concat(innerText.Where(char.IsDigit));
            }
            else
            {
                return null;
            }
        }

        public static string GetInnerDecimal(HtmlDocument html, string xpath, int decimalPlaces = 0, string valorPadrao = "0.00")
        {
            HtmlNode element = html.DocumentNode.SelectSingleNode(xpath);
            string innerText = element?.InnerText;
            if (innerText != null)
            {
                string result = string.Concat(innerText.Where(c => char.IsDigit(c) || char.IsPunctuation(c)));
                if (string.IsNullOrWhiteSpace(result))
                {
                    return valorPadrao;
                }

                if (decimalPlaces > 0)
                {
                    result = Decimal.Parse(result).ToString($"F{decimalPlaces}");
                }

                result = result.Replace(".", "").Replace(",", ".");
                return result;
            }
            else
            {
                return null;
            }
        }

        public static string GetInnerTextCode(HtmlDocument html, string xpath, string valorPadrao = "")
        {
            HtmlNode element = html.DocumentNode.SelectSingleNode(xpath);
            string result = element != null ? element?.InnerText?.Split('-')?.FirstOrDefault()?.Trim() : element?.InnerText;
            if (string.IsNullOrWhiteSpace(result))
            {
                result = valorPadrao;
            }

            return result;
        }

        public static string GetInnerTextValue(HtmlDocument html, string xpath)
        {
            HtmlNode element = html.DocumentNode.SelectSingleNode(xpath);
            if (element != null)
            {
                return element?.InnerText?.Split('-')?.LastOrDefault()?.Trim();
            }
            else
            {
                return element?.InnerText;
            }
        }

        public static string GetImgSrc(HtmlDocument html, string xpath)
        {
            HtmlNode element = html.DocumentNode.SelectSingleNode(xpath);
            return element?.GetAttributeValue("src", "");
        }

        public static string OnlyNumbers(string str)
        {
            return string.Concat(str?.Where(c => char.IsDigit(c)));
        }

        public enum TipoExtracao
        {
            Text,
            Code,
            Decimal,
        }
    }
}

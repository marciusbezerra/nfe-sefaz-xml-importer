﻿using NfeUtils.NfeClasses;
using static NfeUtils.NfeService;
using System;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Windows.Forms;
using System.Xml.Serialization;

namespace WinSefazImporter
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
#if DEBUG
            //textBoxNfNumber.Text = "35180500776574002280550010010156101983838372";
            //textBoxNfNumber.Text = "35171201281020000221550010001496021257312278";

            //textBoxNfNumber.Text = "35180819364681000103550010001110141003155259";
            //textBoxNfNumber.Text = "35180819364681000103550010001110151003154977";
            //textBoxNfNumber.Text = "35180819364681000103550010001110161003154982";
            //textBoxNfNumber.Text = "35180819364681000103550010001110171003154998";
            //textBoxNfNumber.Text = "35180819364681000103550010001110181003155002";
            //textBoxNfNumber.Text = "35180819364681000103550010001110191003155271";
            //textBoxNfNumber.Text = "35180819364681000103550010001110201003155019";
            //textBoxNfNumber.Text = "35180819364681000103550010001110211003155024";
            //textBoxNfNumber.Text = "35180819364681000103550010001110221003155030";
            //textBoxNfNumber.Text = "35180819364681000103550010001110231003155045";
            //textBoxNfNumber.Text = "35180819364681000103550010001110241003155247";
            //textBoxNfNumber.Text = "35180819364681000103550010001110251003155058";
            //textBoxNfNumber.Text = "35180819364681000103550010001110261003155063";
            //textBoxNfNumber.Text = "35180819364681000103550010001110271003155230";
#endif
            buttonTrocarCaptcharClickAsync(buttonTrocarCaptchar, null);
        }

        private CookieContainer cookieContainer;
        private ProcessNfe processoNfe;

        private const string UrlAntiga = "http://www.nfe.fazenda.gov.br/Portal/consultaResumoCompletaAntiga.aspx?tipoConsulta=completa&tipoConteudo=XbSeqxE8pl8=";
        private const string UrlCompleta = "http://www.nfe.fazenda.gov.br/portal/consultaImpressao.aspx?tipoConsulta=completa";
        //private const string UrlCompleta = "http://www.nfe.fazenda.gov.br/portal/consultaCompleta.aspx?tipoConteudo=XbSeqxE8pl8=";

        private async void buttonTrocarCaptcharClickAsync(object sender, EventArgs e)
        {
            Cursor = Cursors.WaitCursor;
            buttonTrocarCaptchar.Enabled = false;
            textBoxNfNumber.Enabled = false;
            textBoxCaptchar.Enabled = false;
            buttonGerarXml.Enabled = false;
            try
            {
                processoNfe = await UpdateCaptchar();

                byte[] imageBytes = Convert.FromBase64String(processoNfe.CaptcharImg.Replace("data:image/png;base64,", ""));
                using (var ms = new MemoryStream(imageBytes, 0, imageBytes.Length))
                {
                    pictureBox1.Image = Image.FromStream(ms, true);
                }

                textBoxCaptchar.Text = "";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                buttonTrocarCaptchar.Enabled = true;
                textBoxNfNumber.Enabled = true;
                textBoxCaptchar.Enabled = true;
                buttonGerarXml.Enabled = true;
                Cursor = Cursors.Default;
            }
        }

        private async Task<ProcessNfe> UpdateCaptchar()
        {
            var responseString = await ProcessaRequestAsync(UrlAntiga, null, "GET");

            HtmlAgilityPack.HtmlDocument html = new HtmlAgilityPack.HtmlDocument();
            html.Load(responseString);

            return new ProcessNfe
            {

                CaptcharImg = GetImgSrc(html, "//*[@id=\"ctl00_ContentPlaceHolder1_imgCaptcha\"]"),
                CaptcharSom = GetInnerValue(html, "//*[@id=\"ctl00_ContentPlaceHolder1_captchaSom\"]"),
                Token = GetInnerValue(html, "//*[@id=\"ctl00_ContentPlaceHolder1_token\"]"),
                ViewState = GetInnerValue(html, "//*[@id=\"__VIEWSTATE\"]"),
                ViewStateGenerator = GetInnerValue(html, "//*[@id=\"__VIEWSTATEGENERATOR\"]"),
                EventValidation = GetInnerValue(html, "//*[@id=\"__EVENTVALIDATION\"]")
            };
        }

        private async Task<Stream> ProcessaRequestAsync(string url, byte[] data, string method)
        {
            const int maxErrors = 12;
            var errCount = 0;
            while (true)
            {
                try
                {
                    if (cookieContainer == null)
                        cookieContainer = new CookieContainer();
                    var request = (HttpWebRequest)WebRequest.Create(url);

                    var sp = request.ServicePoint;
                    var prop = sp.GetType().GetProperty("HttpBehaviour", BindingFlags.Instance | BindingFlags.NonPublic);
                    prop.SetValue(sp, (byte)0, null);

                    sp.Expect100Continue = false;
                    request.Method = method;
                    request.CookieContainer = cookieContainer;
                    request.Timeout = Timeout.Infinite;
                    request.AllowAutoRedirect = false;
                    request.KeepAlive = true;
                    request.Accept = "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8";
                    request.UserAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36";
                    request.Referer = "http://www.nfe.fazenda.gov.br/portal/consultaResumoCompletaAntiga.aspx?tipoConsulta=completa&tipoConteudo=XbSeqxE8pl8=";
                    request.Headers.Add("Accept-Encoding", "gzip, deflate");
                    request.Headers.Add("Accept-Language", "pt-BR,pt;q=0.9,en-US;q=0.8,en;q=0.7,es;q=0.6");
                    request.Headers.Add("Upgrade-Insecure-Requests", "1");
                    request.Headers.Add("Cache-Control", "max-age=0");
                    request.Headers.Add("Origin", "http://www.nfe.fazenda.gov.br");

                    if (data != null)
                    {
                        request.ContentLength = data.Length;
                        request.Host = "www.nfe.fazenda.gov.br";
                        request.ContentType = "application/x-www-form-urlencoded";
                        using (var stream = request.GetRequestStream())
                            stream.Write(data, 0, data.Length);
                    }

                    AddLog($"Iniciando requisição {errCount + 1}: {url}", "info");
                    var response = await request.GetResponseAsync() as HttpWebResponse;
                    AddLog("Requisição finalizada", "Info");
                    //return await new StreamReader(response.GetResponseStream(), Encoding.Default).ReadToEndAsync();
                    return response.GetResponseStream();
                }
                catch (Exception ex)
                {
                    errCount++;
                    AddLog($"Erro ({errCount}): {ex}", "erro");
                    if (errCount >= maxErrors)
                        throw;
                    await Task.Delay(10000);
                }
            }
        }

        private void AddLog(string logMsg, string logType)
        {
            var item = listViewLog.Items.Add(logType);
            item.SubItems.Add(logMsg);
            listViewLog.Columns[0].AutoResize(ColumnHeaderAutoResizeStyle.ColumnContent);
            listViewLog.Columns[0].AutoResize(ColumnHeaderAutoResizeStyle.ColumnContent);
            item.EnsureVisible();
        }

        public static void ExtractInfoDoNumeroDaNf(string numeroNf, out string cUf, out string cNf, out string mod, out string indPag, out string cDV)
        {
            var numeroParts = numeroNf.Split('-');
            cUf = numeroParts.FirstOrDefault();
            cNf = string.Join("", numeroParts[7]?.Where(char.IsDigit));
            mod = numeroParts[4];
            indPag = numeroParts.LastOrDefault();
            cDV = numeroParts.LastOrDefault();
        }

        public static string GetInnerText(HtmlAgilityPack.HtmlDocument html, string xpath)
        {
            var element = html.DocumentNode.SelectSingleNode(xpath);
            return element?.InnerText;
        }

        public static string GetInnerValue(HtmlAgilityPack.HtmlDocument html, string xpath)
        {
            var element = html.DocumentNode.SelectSingleNode(xpath);
            return element?.GetAttributeValue("value", "");
        }

        public static string GetInnerLong(HtmlAgilityPack.HtmlDocument html, string xpath)
        {
            var element = html.DocumentNode.SelectSingleNode(xpath);
            var innerText = element?.InnerText;
            if (innerText != null)
                return string.Concat(innerText.Where(char.IsDigit));
            else
                return null;
        }

        public static string GetInnerDecimal(HtmlAgilityPack.HtmlDocument html, string xpath)
        {
            var element = html.DocumentNode.SelectSingleNode(xpath);
            var innerText = element?.InnerText;
            if (innerText != null)
                return string.Concat(innerText.Where(c => char.IsDigit(c) || char.IsPunctuation(c)));
            else
                return null;
        }

        public static string GetInnerTextCode(HtmlAgilityPack.HtmlDocument html, string xpath)
        {
            var element = html.DocumentNode.SelectSingleNode(xpath);
            if (element != null)
                return element?.InnerText?.Split('-')?.FirstOrDefault()?.Trim();
            else
                return element?.InnerText;
        }

        public static string GetImgSrc(HtmlAgilityPack.HtmlDocument html, string xpath)
        {
            var element = html.DocumentNode.SelectSingleNode(xpath);
            return element?.GetAttributeValue("src", "");
        }

        public static string OnlyNumbers(string str)
        {
            return string.Concat(str?.Where(c => char.IsDigit(c)));
        }

        private async void buttonGerarXmlClickAsync(object sender, EventArgs e)
        {
            Cursor = Cursors.WaitCursor;
            buttonTrocarCaptchar.Enabled = false;
            textBoxNfNumber.Enabled = false;
            textBoxCaptchar.Enabled = false;
            buttonGerarXml.Enabled = false;
            try
            {
                var postData = "__EVENTARGUMENT=&";
                postData += "__EVENTTARGET=&";
                postData += $"__EVENTVALIDATION={HttpUtility.UrlEncode(processoNfe.EventValidation)}&";
                postData += $"__VIEWSTATE={HttpUtility.UrlEncode(processoNfe.ViewState)}&";
                postData += $"__VIEWSTATEGENERATOR={HttpUtility.UrlEncode(processoNfe.ViewStateGenerator)}&";
                postData += "ctl00%24ContentPlaceHolder1%24btnConsultar=Continuar&";
                postData += $"ctl00%24ContentPlaceHolder1%24captchaSom={HttpUtility.UrlEncode(processoNfe.CaptcharSom)}&";
                postData += $"ctl00%24ContentPlaceHolder1%24token={processoNfe.Token}&";
                postData += $"ctl00%24ContentPlaceHolder1%24txtCaptcha={textBoxCaptchar.Text}&";
                postData += $"ctl00%24ContentPlaceHolder1%24txtChaveAcessoCompleta={OnlyNumbers(textBoxNfNumber.Text)}&";
                postData += "ctl00%24txtPalavraChave=&";
                postData += "hiddenInputToUpdateATBuffer_CommonToolkitScripts=1";
                var data = Encoding.ASCII.GetBytes(postData);

                //var responseString1 = ProcessaRequest("http://localhost/SefazXmlImporter/Home/Test?tipoConsulta=completa&tipoConteudo=XbSeqxE8pl8=", data, "POST");
                var responseString1 = await ProcessaRequestAsync(UrlAntiga, data, "POST");

                //return View("Index", processNfe);

                HtmlAgilityPack.HtmlDocument html = new HtmlAgilityPack.HtmlDocument();
                html.Load(responseString1);
                var erro = GetInnerText(html, "//*[@id=\"ctl00_ContentPlaceHolder1_bltMensagensErro\"]/li");

                if (!string.IsNullOrWhiteSpace(erro))
                {
                    MessageBox.Show(HttpUtility.HtmlDecode(erro));
                    return;
                }

                var responseString2 = await ProcessaRequestAsync(UrlCompleta, null, "GET");

                html = new HtmlAgilityPack.HtmlDocument();
                html.Load(responseString2);
                erro = GetInnerText(html, "//*[@id=\"ctl00_ContentPlaceHolder1_lblMensagemErro\"]");

                if (!string.IsNullOrWhiteSpace(erro))
                {
                    MessageBox.Show(HttpUtility.HtmlDecode(erro));
                    return;
                }

                nfeProc nfeProc = GetNfeProc(html);

                saveFileDialog.FileName = $"{OnlyNumbers(textBoxNfNumber.Text)}.xml";

                if (saveFileDialog.ShowDialog(this) == DialogResult.OK)
                {
                    Save(nfeProc, saveFileDialog.FileName);
                    Process.Start(Path.GetDirectoryName(saveFileDialog.FileName));
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                buttonTrocarCaptchar.Enabled = true;
                textBoxNfNumber.Enabled = true;
                textBoxCaptchar.Enabled = true;
                buttonGerarXml.Enabled = false;
                Cursor = Cursors.Default;
            }
        }
    }

    public class ProcessNfe
    {
        public string Token { get; set; }
        public string NfNumber { get; set; }
        public string CaptcharTxt { get; set; }
        public string CaptcharImg { get; set; }
        public string CaptcharSom { get; set; }
        public string ViewState { get; set; }
        public string EventValidation { get; set; }
        public string ViewStateGenerator { get; set; }
        public string Xml { get; set; }
    }
}

﻿namespace WinSefazImporter
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.textBoxNfNumber = new System.Windows.Forms.TextBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.textBoxCaptchar = new System.Windows.Forms.TextBox();
            this.buttonTrocarCaptchar = new System.Windows.Forms.Button();
            this.buttonGerarXml = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.listViewLog = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.saveFileDialog = new System.Windows.Forms.SaveFileDialog();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // textBoxNfNumber
            // 
            this.textBoxNfNumber.Location = new System.Drawing.Point(12, 28);
            this.textBoxNfNumber.Name = "textBoxNfNumber";
            this.textBoxNfNumber.Size = new System.Drawing.Size(460, 20);
            this.textBoxNfNumber.TabIndex = 0;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(12, 55);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(281, 49);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            // 
            // textBoxCaptchar
            // 
            this.textBoxCaptchar.Location = new System.Drawing.Point(12, 123);
            this.textBoxCaptchar.Name = "textBoxCaptchar";
            this.textBoxCaptchar.Size = new System.Drawing.Size(281, 20);
            this.textBoxCaptchar.TabIndex = 2;
            // 
            // buttonTrocarCaptchar
            // 
            this.buttonTrocarCaptchar.Location = new System.Drawing.Point(299, 55);
            this.buttonTrocarCaptchar.Name = "buttonTrocarCaptchar";
            this.buttonTrocarCaptchar.Size = new System.Drawing.Size(173, 41);
            this.buttonTrocarCaptchar.TabIndex = 3;
            this.buttonTrocarCaptchar.Text = "Trocar Captchar";
            this.buttonTrocarCaptchar.UseVisualStyleBackColor = true;
            this.buttonTrocarCaptchar.Click += new System.EventHandler(this.buttonTrocarCaptcharClickAsync);
            // 
            // buttonGerarXml
            // 
            this.buttonGerarXml.Location = new System.Drawing.Point(299, 102);
            this.buttonGerarXml.Name = "buttonGerarXml";
            this.buttonGerarXml.Size = new System.Drawing.Size(173, 41);
            this.buttonGerarXml.TabIndex = 3;
            this.buttonGerarXml.Text = "Gerar XML";
            this.buttonGerarXml.UseVisualStyleBackColor = true;
            this.buttonGerarXml.Click += new System.EventHandler(this.buttonGerarXmlClickAsync);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(76, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Nº do DANFE:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 107);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Captchar:";
            // 
            // listViewLog
            // 
            this.listViewLog.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2});
            this.listViewLog.Location = new System.Drawing.Point(12, 150);
            this.listViewLog.Name = "listViewLog";
            this.listViewLog.Size = new System.Drawing.Size(460, 146);
            this.listViewLog.TabIndex = 5;
            this.listViewLog.UseCompatibleStateImageBehavior = false;
            this.listViewLog.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "*";
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Satus";
            this.columnHeader2.Width = 350;
            // 
            // saveFileDialog
            // 
            this.saveFileDialog.DefaultExt = "xml";
            this.saveFileDialog.Filter = "Arquivos Xml|*.xml|Todos os tipos de arquivos|*.*";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(484, 308);
            this.Controls.Add(this.listViewLog);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.buttonGerarXml);
            this.Controls.Add(this.buttonTrocarCaptchar);
            this.Controls.Add(this.textBoxCaptchar);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.textBoxNfNumber);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.Text = "XML NFE";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBoxNfNumber;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.TextBox textBoxCaptchar;
        private System.Windows.Forms.Button buttonTrocarCaptchar;
        private System.Windows.Forms.Button buttonGerarXml;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ListView listViewLog;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.SaveFileDialog saveFileDialog;
    }
}


﻿using System.ComponentModel.DataAnnotations;
using System.Net;

namespace SefazXmlImporter.Models
{
    public class ProcessNfe
    {
        public string Token { get; set; }

        [Display(Name = "Nº da Nota"), Required]
        public string NfNumber { get; set; }

        [Display(Name = "Texto do Captchar"), Required]
        public string CaptcharTxt { get; set; }

        [Display(Name = "Imagem")]
        public string CaptcharImg { get; set; }

        [Display(Name = "Som do Captchar"), Required]
        public string CaptcharSom { get; set; }

        public string ViewState { get; set; }

        public string EventValidation { get; set; }

        public string ViewStateGenerator { get; set; }

        public string Xml { get; set; }
    }
}
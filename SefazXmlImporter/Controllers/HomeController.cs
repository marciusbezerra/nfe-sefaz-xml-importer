﻿using System.Web.Mvc;
using HtmlAgilityPack;
using System.Net;
using System.Text;
using System.IO;
using SefazXmlImporter.Models;
using System;
using System.Threading;
using System.Diagnostics;
using System.Web;
using static NfeUtils.NfeService;
using NfeUtils.NfeClasses;

namespace SefazXmlImporter.Controllers
{
    public class HomeController : Controller
    {
        private const string UrlAntiga = "http://www.nfe.fazenda.gov.br/Portal/consultaResumoCompletaAntiga.aspx?tipoConsulta=completa&tipoConteudo=XbSeqxE8pl8=";
        //private const string UrlCompleta = "http://www.nfe.fazenda.gov.br/portal/consultaCompleta.aspx?tipoConteudo=XbSeqxE8pl8=";
        private const string UrlCompleta = "http://www.nfe.fazenda.gov.br/portal/consultaImpressao.aspx?tipoConsulta=completa";

        public ActionResult Index()
        {
            var processRequest = new ProcessNfe();
            try
            {
                var responseString = ProcessaRequest(UrlAntiga, null, "GET");

                HtmlDocument html = new HtmlDocument();
                html.LoadHtml(responseString);

                processRequest.CaptcharImg = GetImgSrc(html, "//*[@id=\"ctl00_ContentPlaceHolder1_imgCaptcha\"]");
                processRequest.CaptcharSom = GetInnerValue(html, "//*[@id=\"ctl00_ContentPlaceHolder1_captchaSom\"]");
                processRequest.Token = GetInnerValue(html, "//*[@id=\"ctl00_ContentPlaceHolder1_token\"]");
                processRequest.ViewState = GetInnerValue(html, "//*[@id=\"__VIEWSTATE\"]");
                processRequest.ViewStateGenerator = GetInnerValue(html, "//*[@id=\"__VIEWSTATEGENERATOR\"]");
                processRequest.EventValidation = GetInnerValue(html, "//*[@id=\"__EVENTVALIDATION\"]");

                return View(processRequest);
            }
            catch (Exception ex)
            {
                TempData["Error"] = ex.Message;
                return View(processRequest);
            }
        }

        private string ProcessaRequest(string url, byte[] data, string method)
        {
            const int maxErrors = 12;
            var errCount = 0;
            while (true)
            {
                try
                {
                    if (Session["cookieContainer"] == null)
                        Session["cookieContainer"] = new CookieContainer();
                    var cookieContainer = (CookieContainer)Session["cookieContainer"];
                    var request = (HttpWebRequest)WebRequest.Create(url);
                    request.ServicePoint.Expect100Continue = false;
                    request.Method = method;
                    request.CookieContainer = cookieContainer;
                    request.Timeout = Timeout.Infinite;
                    request.KeepAlive = true;
                    request.Accept = "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8";
                    request.UserAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36";
                    request.Referer = "http://www.nfe.fazenda.gov.br/portal/consultaResumoCompletaAntiga.aspx?tipoConsulta=completa&tipoConteudo=XbSeqxE8pl8=";
                    request.Headers.Add("Accept-Encoding", "gzip, deflate");
                    request.Headers.Add("Accept-Language", "pt-BR,pt;q=0.9,en-US;q=0.8,en;q=0.7,es;q=0.6");
                    request.Headers.Add("Upgrade-Insecure-Requests", "1");
                    request.Headers.Add("Cache-Control", "max-age=0");
                    request.Headers.Add("Origin", "http://www.nfe.fazenda.gov.br");

                    if (data != null)
                    {
                        request.ContentLength = data.Length;
                        request.Host = "www.nfe.fazenda.gov.br";
                        request.ContentType = "application/x-www-form-urlencoded";
                        using (var stream = request.GetRequestStream())
                            stream.Write(data, 0, data.Length);
                    }

                    var response = (HttpWebResponse)request.GetResponse();
                    var responseString = new StreamReader(response.GetResponseStream(), Encoding.Default).ReadToEnd();
                    Session["cookieContainer"] = request.CookieContainer;
                    return responseString;
                }
                catch (Exception ex)
                {
                    errCount++;
                    Debug.WriteLine($"errCount: {errCount}: {ex}");
                    if (errCount >= maxErrors)
                        throw;
                    Thread.Sleep(10000);
                }
            }
        }

        [HttpPost]
        public ActionResult ProcessaNfe(ProcessNfe processNfe)
        {
            try
            {
                if (!ModelState.IsValid) return View("Index", processNfe);

                var postData = "__EVENTARGUMENT=&";
                postData += "__EVENTTARGET=&";
                postData += $"__EVENTVALIDATION={HttpUtility.UrlEncode(processNfe.EventValidation)}&";
                postData += $"__VIEWSTATE={HttpUtility.UrlEncode(processNfe.ViewState)}&";
                postData += $"__VIEWSTATEGENERATOR={HttpUtility.UrlEncode(processNfe.ViewStateGenerator)}&";
                postData += "ctl00%24ContentPlaceHolder1%24btnConsultar=Continuar&";
                postData += $"ctl00%24ContentPlaceHolder1%24captchaSom={HttpUtility.UrlEncode(processNfe.CaptcharSom)}&";
                postData += $"ctl00%24ContentPlaceHolder1%24token={processNfe.Token}&";
                postData += $"ctl00%24ContentPlaceHolder1%24txtCaptcha={processNfe.CaptcharTxt}&";
                postData += $"ctl00%24ContentPlaceHolder1%24txtChaveAcessoCompleta={OnlyNumbers(processNfe.NfNumber)}&";
                postData += "ctl00%24txtPalavraChave=&";
                postData += "hiddenInputToUpdateATBuffer_CommonToolkitScripts=1";
                var data = Encoding.ASCII.GetBytes(postData);

                var responseString1 = ProcessaRequest(UrlAntiga, data, "POST");

                HtmlDocument html = new HtmlDocument();
                html.LoadHtml(responseString1);
                var erro = GetInnerText(html, "//*[@id=\"ctl00_ContentPlaceHolder1_bltMensagensErro\"]/li");

                if (!string.IsNullOrWhiteSpace(erro))
                {
                    TempData["Error"] = HttpUtility.HtmlDecode(erro);
                    return RedirectToAction("Index");
                }

                var responseString2 = ProcessaRequest(UrlCompleta, null, "GET");

                html = new HtmlDocument();
                html.LoadHtml(responseString2);
                erro = GetInnerText(html, "//*[@id=\"ctl00_ContentPlaceHolder1_lblMensagemErro\"]");

                if (!string.IsNullOrWhiteSpace(erro))
                {
                    TempData["Error"] = HttpUtility.HtmlDecode(erro);
                    return RedirectToAction("Index");
                }

                nfeProc nfeProc = GetNfeProc(html);

                var processResult = new ProcessNfe
                {
                    NfNumber = processNfe.NfNumber,
                    CaptcharTxt = processNfe.CaptcharTxt,
                    Xml = ObjectToXml(nfeProc)
                };

                return View(processResult);
            }
            catch (Exception ex)
            {
                TempData["Error"] = ex.Message;
                return RedirectToAction("Index");
            }
        }

        [HttpPost, ValidateInput(false)]
        public FileResult DownloadXml(ProcessNfe processNfe)
        {
            var xml = processNfe.Xml;
            var filename = $"{processNfe.NfNumber}.xml";
            return File(Encoding.UTF8.GetBytes(xml), "application/xml", filename);
        }

        public ActionResult About()
        {
            ViewBag.Message = "SEFAZ - Importador de XML da Nota Fiscal (por Marcius Bezerra)";
            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Marcius Bezerra";
            return View();
        }
    }
}
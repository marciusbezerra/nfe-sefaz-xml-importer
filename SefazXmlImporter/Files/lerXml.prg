#include "fivewin.ch"
#include "image.ch"
#include "directry.ch"
#include "fileio.ch"
#include "tip.ch"

#define URL_CONSULTA_COMPLETA  "https://www.nfe.fazenda.gov.br/portal/consultaCompleta.aspx?tipoConteudo=XbSeqxE8pl8="
#define URL_CONSULTA           "https://www.nfe.fazenda.gov.br/portal/consultaResumoCompletaAntiga.aspx?tipoConsulta=completa&tipoConteudo=XbSeqxE8pl8%3D"


Static s_cVersao := "", s_cTpAmb      := "", s_cVerAplic      := "", s_cDhRecbto := "", s_cnProt := "", s_cDigVal := ""
Static s_cinfCpl := "", s_cInfAdFisco := "", s_cDiscAdicional := "", s_cxMotivo  := "", s_ccStat := ""
Static s_aIde    := {}, s_aEmit       := {}, s_aDest          := {}, s_aDet      := {}, s_aICMS  := {}, s_aIPI    := {}
Static s_aTransp := {}, s_aCobr       := {}, s_aMedicamentos  := {}, s_aTotais   := {}, s_aPIS   := {}, s_aCOFINS := {}
Static s_cViewState, s_cViewStateGenerator, s_cHiddenToken, s_cEventValidation, s_cStringCookie, s_cHiddenSom

/******************************************************************************/
procedure DownloadNFe( cChave, cPasta )
/*
*/
   local oDialog, oIcone, oFonte, hInput := Hash()
   local cCaptcha       := Space( 6 )
   local lSemParametros := Empty( PCount() )

   if lSemParametros
      cChave := Space( 44 )
      cPasta := ''
   endif

   define font oFonte name 'Arial' size 6, 16 bold
   define icon oIcone resource 'MAINSG'

   define dialog oDialog title 'Download XML - NFe' from 0, 0 to 164, 565  pixel font oFonte icon oIcone

   @  7, 112 say   'Chave:'                              of oDialog pixel
   @ 34, 112 say   'Captcha:'                            of oDialog pixel
   @ 16, 112 get   hInput['Chave']   var cChave          of oDialog pixel size 165, 13 picture '@9' //Valid ValidaChaveDownloadXML( cChave )
   @ 43, 112 get   hInput['Captcha'] var cCaptcha        of oDialog pixel size  34, 13 picture '@!'
   @  8,   4 image hInput['Imagem']                      of oDialog pixel size 104, 48 adjust

   @ 64, 174 button '&Baixar'                            of oDialog pixel size  45, 12 action (MsgRun( 'Aguarde! Realizando Download do XML...',, {|| BaixarXML(cChave, cCaptcha, hInput, oDialog, lSemParametros, cPasta)}))
   @ 64,   4 button '&Atualizar Captcha'                 of oDialog pixel size  64, 12 action (AtualizaCaptcha( hInput ))
   @ 64, 226 button '&Sair'                              of oDialog pixel size  45, 12 action (oDialog:End()) cancel

   oDialog:lHelpIcon := .f.
   activate dialog oDialog centered on init ( IIf( lSemParametros,, hInput['Chave']:disable() ), ;
                                              AtualizaCaptcha( hInput ) )

   oIcone:end()
   oFonte:end()

return

/***********************************************************************************/
static procedure AtualizaCaptcha( hInput )
/*
*/
   MsgRun('Aguarde...',,{|| AtualizaImagem( hInput ) } )
return

/***********************************************************************************/
static procedure AtualizaImagem( hInput )
/*
*/
   Local oServer, cHtml, nInie, cHtml1, nFime, cMsgErro, nIni, nFim, nIni1, nConta := 0
   Local nFim1, nIni2, nFim2, nIni3, nFim3, nIni4, nFim4, cFileString1, cFileString2

   do while nConta <= 5

      nConta ++
      
      
      try
        oServer:= CreateObject( "MSXML2.ServerXMLHTTP.6.0" ) 
      catch
         try
            oServer:= CreateObject( "MSXML2.ServerXMLHTTP.5.0" ) 
         catch
         end
      end

      Try
         oServer:Open( "GET", URL_CONSULTA, .f. )
         oServer:SetRequestHeader( "Content-Type", "application/x-www-form-urlencoded" )
         oServer:SetRequestHeader( "Connection", "keep-alive" )
         oServer:Send()
         oServer:WaitForResponse( 10000 )
      Catch
         MsgInfo('Erro na conexão com o site!', 'Atenção!')
         exit
      End

      cHtml           := oServer:ResponseBody
      s_cStringCookie := oServer:getResponseHeader("Set-Cookie")
      nInie           := At('id="ctl00_ContentPlaceHolder1_lblMensagemErro">', cHtml) + 47

      nArqHandle := FCreate("cHtml.TXT", 0 )
   
      FWrite(nArqHandle,cHtml,LEN(cHtml))
      FClose(nArqHandle)

      If nInie > 47
         cHtml1   := SubStr( cHtml, nInie)
         nFime    := At('</span>', cHtml1) - 1
         cMsgErro := SubStr(cHtml, nInie, nFime)
         If !Empty(cMsgErro)
            MsgInfo(cMsgErro, 'Erro no Site ' + URL_CONSULTA)
            exit
         EndIf
      EndIf

      nIni                  := At('id="__VIEWSTATE" value=', cHtml) + 24
      cHtml1                := SubStr( cHtml, nIni)
      nFim                  := At('" />', cHtml1) - 1
      s_cViewState          := SubStr(cHtml, nIni, nFim)

      nIni1                 := At('id="__EVENTVALIDATION" value="', cHtml) + 30
      cHtml1                := SubStr( cHtml, nIni1)
      nFim1                 := At('" />', cHtml1) - 1
      s_cEventValidation    := SubStr(cHtml, nIni1, nFim1)

      nIni2                 := At('id="__VIEWSTATEGENERATOR" value="', cHtml) + 33
      cHtml1                := SubStr( cHtml, nIni2)
      nFim2                 := At('" />', cHtml1) - 1
      s_cViewStateGenerator := SubStr(cHtml, nIni2, nFim2)

      nIni3                 := At('id="ctl00_ContentPlaceHolder1_token" value="', cHtml) + 44
      cHtml1                := SubStr( cHtml, nIni3)
      nFim3                 := At('" />', cHtml1) - 1
      s_cHiddenToken        := SubStr(cHtml, nIni3, nFim3)

      nIni4                 := At('id="ctl00_ContentPlaceHolder1_captchaSom" value="', cHtml) + 49
      cHtml1                := SubStr( cHtml, nIni4)
      nFim4                 := At('" />', cHtml1) - 1
      s_cHiddenSom          := SubStr(cHtml, nIni4, nFim4)

      nIni                  := At('data:image/png;base64,', cHtml) + 22
      cHtml1                := SubStr(cHtml, nIni)
      
      for i=1 to len(cHtml1)
          if substr(cHtml1,i,1)=chr(34) .and. substr(cHtml1,i+1,3)=" />"
             nFim:=i+1
             i:=len(cHtml1)+10
          endif
      next
      
**      nFim                  := At('" style="border-width:0px;" />', cHtml1) - 2
      cFileString1          := SubStr(cHtml, nIni, nFim)
      cFileString2          := HB_Base64Decode(cFileString1)

      If Empty(cFileString1)
         loop
      EndIf

      if Upper(Right(ALLTRIM(cFileString1),5))=='SUQMC'
         SysWait(0.5)
         loop
      endif

      if HB_IsObject( hInput['Imagem'] )
         hInput['Imagem']:LoadFromMemory( cFileString2 )
         hInput['Imagem']:Refresh()
      endif

      exit

   enddo

   if nConta > 5
      MsgStop('Erro ao tentar obter CAPTCHA. Tente novamente!', 'Atenção!')
   endif

Return

/******************************************************************************************/
static function BaixarXML( cChave, cCaptcha, hInput, oDialog, lSemParametros, cPasta )
/*
*/
   Local p, oInternet, cParm, cRetorno := '', cHtml := '', c_span := ''
   Local nIni, cPart, cParte, nFim, cFile, nTamTag, nTotPro

   if Len( Alltrim(cChave) ) < 44
      MsgStop( 'Chave tem que ter 44 digitos!', 'Atenção!')
      hInput['Chave']:setFocus()
      return
   endif

   if Len( Alltrim(cCaptcha) ) < 6
      MsgStop( 'Favor informar o captcha corretamente!', 'Atenção!')
      hInput['Captcha']:setFocus()
      return
   endif

   // Conteudo elementos da Matriz = {VALOR_TAG,TAG,TAG_PAI,Elemento_Final_TAG_PAI,LABEL_HTML,TAM_LABEL}
   // TAM_LABEL se ZERO pega tudo, se MAIOR que ZERO pega o tamanho no início e se MENOR que ZERO exclui o tamanho no início e pega o restante

   AADD(s_aIde,{{"","cUF","ide",21,"Chave de Acesso",2},; //1- tratamento especial
                {"","cNF","",0,"Número",0},; //2- tratamento especial
                {"","natOp","",0,"Natureza da Operação",0},; //3-
                {"","indPag","",0,"Forma de Pagamento",1},; //4-
                {"","mod","",0,"Modelo",0},; //5-
                {"","serie","",0,"Série",0},; //6-
                {"","nNF","",0,"Número",0},; //7-
                {"","dhEmi","",0,"Data de Emissão",0},; //8- tratamento especial
                {"","dhSaiEnt","",0,"Entrada",0},; //9- tratamento especial
                {"","tpNF","",0,"Tipo da Operação",1},; //10-
                {"","idDest","",0,"Destino da operação",1},; //11-
                {"","cMunFG","",0,"Município da Ocorrência do Fato Gerador do ICMS",0},; //12-
                {"","tpImp","",0,"Formato de Impressão DANFE",1},; //13- tratamento especial
                {"","tpEmis","",0,"Tipo de Emissão",1},; //14-
                {"","cDV","",0,"Chave de Acesso",0},; //15- tratamento especial
                {"","tpAmb","",0,"Ambiente de autorização:",0},; //16- tratamento especial
                {"","finNFe","",0,"Finalidade",1},; //17-
                {"","indFinal","",0,"Consumidor final",1},; //18-
                {"","indPres","",0,"Presença do Comprador",1},; //19-
                {"","procEmi","",0,"Processo",1},; //20-
                {"","verProc","",0,"Versão do Processo",0}}) //21-

   AADD(s_aEmit,{{"","CNPJ","emit",20,"CNPJ",0},; //1- tratamento especial
                 {"","CPF","",0,"CPF",0},; //2-
                 {"","xNome","",0,"Nome / Razão Social",0},; //3-
                 {"","xFant","",0,"Nome Fantasia",0},; //4-
                 {"","xLgr","enderEmit",15,"Endereço",0},; //5- tratamento especial
                 {"","nro","",0,"Endereço",0},; //6- tratamento especial
                 {"","xCpl","",0,"Endereço",0},; //7- tratamento especial
                 {"","xBairro","",0,"Bairro / Distrito",0},; //8-
                 {"","cMun","",0,"Município",7},; //9-
                 {"","xMun","",0,"Município",-10},; //10-
                 {"","UF","",0,"UF",0},; //11-
                 {"","CEP","",0,"CEP",0},; //12- tratamento especial
                 {"","cPais","",0,"País",4},; //13-
                 {"","xPais","",0,"País",-7},; //14-
                 {"","fone","",0,"Telefone",0},; //15- tratamento especial
                 {"","IE","",0,"Inscrição Estadual",0},; //16-
                 {"","IEST","",0,"Inscrição Estadual do Substituto Tributário",0},; //17-
                 {"","IM","",0,"Inscrição Municipal",0},; //18-
                 {"","CNAE","",0,"CNAE Fiscal",0},; //19-
                 {"","CRT","",0,"Código de Regime Tributário",1}}) //20-

   AADD(s_aDest,{{"","CNPJ","dest",20,"CNPJ",0},; //1-
                 {"","CPF","",0,"CPF",0},; //2-
                 {"","idEstrangeiro","",0,"",0},; //3-
                 {"","xNome","",0,"Nome / Razão Social",0},; //4-
                 {"","xLgr","enderDest",15,"Endereço",0},; //5- tratamento especial
                 {"","nro","",0,"Endereço",0},; //6- tratamento especial
                 {"","xCpl","",0,"Endereço",0},; //7- tratamento especial
                 {"","xBairro","",0,"Bairro / Distrito",0},; //8-
                 {"","cMun","",0,"Município",7},; //9-
                 {"","xMun","",0,"Município",-10},; //10-
                 {"","UF","",0,"UF",0},; //11-
                 {"","CEP","",0,"CEP",0},; //12- tratamento especial
                 {"","cPais","",0,"País",4},; //13-
                 {"","xPais","",0,"País",-7},; //14-
                 {"","fone","",0,"Telefone",0},; //15- tratamento especial
                 {"","indIEDest","",0,"Indicador IE",2},; //16- tratamento especial
                 {"","IE","",0,"Inscrição Estadual",0},; //17-
                 {"","ISUF","",0,"Inscrição SUFRAMA",0},; //18-
                 {"","IM","",0,"IM",0},; //19-
                 {"","email","",0,"E-mail",0}}) //20-

   //Conteudo elementos da Matriz = {VALOR_TAG,TAG,LABEL_HTML}

   AADD(s_aTotais,{{"0.00","vBC","Base de Cálculo ICMS"},; //1-
                   {"0.00","vICMS","Valor do ICMS"},; //2-
                   {"0.00","vICMSDeson","Valor do ICMS Desonerado"},; //3-
                   {"0.00","vFCPUFDest","Valor Total ICMS FCP"},; //4-
                   {"0.00","vBCST","Base de Cálculo ICMS ST"},; //5-
                   {"0.00","vST","Valor ICMS Substituição"},; //6-
                   {"0.00","vProd","Valor Total dos Produtos"},; //7-
                   {"0.00","vFrete","Valor do Frete"},; //8-
                   {"0.00","vSeg","Valor do Seguro"},; //9-
                   {"0.00","vDesc","Valor Total dos Descontos"},; //10-
                   {"0.00","vII","Valor Total do II"},; //11-
                   {"0.00","vIPI","Valor Total do IPI"},; //12-
                   {"0.00","vPIS","Valor do PIS"},; //13-
                   {"0.00","vCOFINS","Valor da COFINS"},; //14-
                   {"0.00","vOutro","Outras Despesas Acessórias"},; //15-
                   {"0.00","vNF","Valor Total da NFe"}}) //16-

   //Conteudo elementos da Matriz = {VALOR_TAG,TAG,TAG_PAI,Elemento_Final_TAG_PAI,LABEL_HTML,TAM_LABEL}

   AADD(s_aTransp,{{"","modFrete","transp",17,"Modalidade do Frete",1},; //1-
                   {"","CNPJ","transporta",8,"CNPJ",0},; //2-
                   {"","CPF","",0,"CPF",0},; //3-
                   {"","xNome","",0,"Razão Social / Nome",0},; //4-
                   {"","IE","",0,"Inscrição Estadual",0},; //5-
                   {"","xEnder","",0,"Endereço Completo",0},; //6-
                   {"","xMun","",0,"Município",0},; //7-
                   {"","UF","",0,"UF",0},; //8-
                   {"",'placa','veicTransp',11,'Placa',0},; //9-
                   {"","UF","",0,"UF",0},; //10-
                   {"","RNTC","",0,"RNTC",0},; //11-
                   {"","qVol","vol",17,"Quantidade",0},; //12-
                   {"","esp","",0,"Espécie",0},; //13-
                   {"","marca","",0,"Marca dos Volumes",0},; //14-
                   {"","nVol","",0,"Volume",0},; //15-
                   {"","pesoL","",0,"Peso Líquido",0},; //16-
                   {"","pesoB","",0,"Peso Bruto",0}}) //17-

   //Conteudo elementos da Matriz = {VALOR_TAG,TAG}

   AADD(s_aCobr,{{"","nDup"},; //1-
                 {"","dVenc"},; //2-
                 {"","vDup"},; //3-
                 {"","nDup"},; //4-
                 {"","dVenc"},; //5-
                 {"","vDup"},; //6-
                 {"","nDup"},; //7-
                 {"","dVenc"},; //8-
                 {"","vDup"},; //9-
                 {"","nDup"},; //10-
                 {"","dVenc"},; //11-
                 {"","vDup"},; //12-
                 {"","nDup"},; //13-
                 {"","dVenc"},; //14-
                 {"","vDup"},; //15-
                 {"","nDup"},; //16-
                 {"","dVenc"},; //17-
                 {"","vDup"},; //18-
                 {"","nDup"},; //19-
                 {"","dVenc"},; //20-
                 {"","vDup"},; //21-
                 {"","nDup"},; //22-
                 {"","dVenc"},; //23-
                 {"","vDup"},; //24-
                 {"","nDup"},; //25-
                 {"","dVenc"},; //26-
                 {"","vDup"},; //27-
                 {"","nDup"},; //28-
                 {"","dVenc"},; //29-
                 {"","vDup"}}) //30-

   If !DigitoChave(cChave)
      Return .f.
   EndIf

   cParm := {{ "__EVENTARGUMENT", "" },;
             { "__EVENTTARGET", "" },;
             { "__EVENTVALIDATION", s_cEventValidation },;
             { "__VIEWSTATE", s_cViewState },;
             { "__VIEWSTATEGENERATOR", s_cViewStateGenerator },;
             { "ctl00$ContentPlaceHolder1$btnConsultar", "Continuar" },;
             { "ctl00$ContentPlaceHolder1$captchaSom", s_cHiddenSom},;
             { "ctl00$ContentPlaceHolder1$token", s_cHiddenToken },;
             { "ctl00$ContentPlaceHolder1$txtCaptcha", cCaptcha },;
             { "ctl00$ContentPlaceHolder1$txtChaveAcessoCompleta", cChave },;
             { "ctl00$txtPalavraChave", "" },;
             { "hiddenInputToUpdateATBuffer_CommonToolkitScripts", "1"}}

   Try
      oInternet := TIPClientHttp():New(URL_CONSULTA_COMPLETA)
   Catch
      MsgInfo("Erro na Criação do Serviço!", "1-Atenção!")
      Return .f.
   End

   oInternet:SetCookie(s_cStringCookie)

   If oInternet:Open()
      If oInternet:POST(cParm)
         cRetorno := oInternet:ReadAll()
      Else
         MsgInfo("Erro no Post " + URL_CONSULTA, "Atenção!")
         Return .f.
      EndIf
   Else
      MsgInfo("Erro na Conexão!", "Atenção!")
      Return .f.
   EndIf

   oInternet:Close()

   If At('id="ctl00_ContentPlaceHolder1_bltMensagensErro" class="listaErro">', cRetorno) > 0
      cPart := Substr(cRetorno, (At('id="ctl00_ContentPlaceHolder1_bltMensagensErro" class="listaErro">', cRetorno) + 66))
      cPart := Substr(cPart, (At('<li>', cPart) + 4))
      MsgInfo(Troca_Acento(SubStr(cPart, 1, (At('</li>', cPart) - 1))), "Atenção!")
      Return .f.
   EndIf

   Try
      oInternet := TIPClientHTTP():New(URL_CONSULTA_COMPLETA)
      oInternet:nConnTimeout := 10000
   Catch
      MsgInfo('Erro na Criação do Serviço', '2-Atenção!')
      Return .f.
   End

   oInternet:SetCookie(s_cStringCookie)

   If oInternet:Open()
      cHtml           := oInternet:ReadAll()
      s_cStringCookie := oInternet:GetCookies()
   Else
      MsgInfo('Erro na Conexão!', 'Atenção!')
      Return .f.
   EndIf

   oInternet:Close()

   If At('id="ctl00_ContentPlaceHolder1_lblResultadoConsulta">', cHtml) > 0
      cPart := Substr(cHtml, (At('id="ctl00_ContentPlaceHolder1_lblResultadoConsulta">', cHtml) + 52))
      MsgInfo(SubStr(cPart, 1, (At('</span>', cPart) - 1)), 'Atenção!')
      Return .f.
   EndIf

   If !Empty(cHtml)
      nIni    := At('<html><link ', cHtml)
      cParte  := SubStr(cHtml, nIni)
      nIni    := At('<div id="ctl00_ContentPlaceHolder1_divBotoesConsulta"', cParte)
      cParte  := SubStr(cParte, 1, nIni - 1)
      nFim    := At('</form>', cParte)
      cFile   := SubStr(cParte, nFim)
      //memowrit('site.txt',cfile)
      nIni    := RAt('fixo-prod-serv-numero',cFile)
      c_span  := SubStr(cFile, nIni)
      nIni    := AT('<span>',c_span)+6
      nTamTag := AT('</span>',c_span)-nIni
      nTotPro := Val(alltrim(SubStr(c_span,nIni,nTamTag)))

      //Conteudo elementos da Matriz = {VALOR_TAG,TAG,LABEL_HTML,TAM_LABEL}
      For p := 1 to nTotPro
          AADD(s_aDet,{{"","cProd","Código do Produto",0},; //1-
                       {"","cEAN","Código EAN Comercial",0},; //2-
                       {"","xProd","fixo-prod-serv-descricao",0},; //3-
                       {"","NCM","Código NCM",0},; //4-
                       {"","NVE","",0},; //5-
                       {"","CEST","Código CEST",0},; //6-
                       {"","EXTIPI","Código EX da TIPI",0},; //7-
                       {"","CFOP","CFOP",0},; //8-
                       {"","uCom","Unidade Comercial",0},; //9-
                       {"","qCom","Quantidade Comercial",0},; //10-
                       {"","vUnCom","Valor unitário de comercialização",0},; //11-
                       {"","vProd","fixo-prod-serv-vb",0},; //12-
                       {"","cEANTrib","Código EAN Tributável",0},; //13-
                       {"","uTrib","Unidade Tributável",0},; //14-
                       {"","qTrib","Quantidade Tributável",0},; //15-
                       {"","vUnTrib","Valor unitário de tributação",0},; //16-
                       {"","vFrete","Valor Total do Frete",0},; //17-
                       {"","vSeg","Valor do Seguro",0},; //18-
                       {"","vDesc","Valor do Desconto",0},; //19-
                       {"","vOutro","Outras Despesas Acessórias",0},; //20-
                       {"","indTot","Indicador de Composição do Valor Total da NF-e",1},; //21-
                       {"","nItemPed","Item do pedido de compra",0}}) //22-

         AADD(s_aMedicamentos, {{"","Lote",'Nro. do Lote', 0},; //1-
                                {"","qLote",'Quantidade de produtos no lote', 0},; //2-
                                {"","dFab",'Data de fabricaçã', 0},; //3-
                                {"","dVal",'Data de validade', 0},; //4-
                                {"","vPMC",'Preço Máximo Consumido', 0},; //5-
                                {"","infAdProd",'Descrição', 0}}) //6-

         AADD(s_aICMS,{{"","orig","Origem da Mercadoria",1},; //1- ICMS Normal e ST
                       {"","CST","Tributação do ICMS",2},; //2-
                       {"","CSOSN","Código de Situação da Operação",3},; //3-
                       {"","modBC","Modalidade Definição da BC do ICMS",1},; //4-
                       {"","pRedBC","Percentual Redução de BC do ICMS",0},; //5-
                       {"","vBC","Base de Cálculo",0},; //6-
                       {"","pICMS","Alíquota",0},; //7-
                       {"","vICMS","Valor",0},; //8-
                       {"","vICMSDeson","",0},; //9-
                       {"","motDesICMS","",0},; //10-
                       {"","modBCST","Modalidade de determinação da BC do ICMS ST",1},; //11-
                       {"","pMVAST","Percentual Margem Valor Adicionado do ICMS ST",0},; //12-
                       {"","pRedBCST","Percentual da Redução de BC do ICMS ST",0},; //13-
                       {"","vBCST","Valor da BC do ICMS ST",0},; //14-
                       {"","pICMSST","Alíquota do imposto do ICMS ST",0},; //15-
                       {"","vICMSST","Valor do ICMS ST",0},; //16-
                       {"","vBCSTRet","Valor da BC do ICMS ST retido",0},; //17-
                       {"","vICMSSTRet","Valor do ICMS ST retido",0},; //18-
                       {"","vICMSOp","",0},; //19-
                       {"","pDif","",0},; //20-
                       {"","vICMSDif","",0},; //21-
                       {"","pCredSN","Alíquota aplicável de cálculo do crédito",0},; //22-
                       {"","vCredICMSSN","Valor crédito do ICMS",0}}) //23-

         AADD(s_aIPI,{{"","cEnq","Código de Enquadramento", 3},;
                      {"","CST","CST", 2},; //1-
                      {"","vBC","Base de Cálculo", 0},; //2-
                      {"","pIPI","Alíquota", 0},; //3-
                      {"","vIPI","Valor IPI", 0},; //4-
                      {"","qBCProd","", 0},; //5-
                      {"","vAliqProd","", 0}}) //6-

         AADD(s_aPIS,{{"","CST","CST",2},; //1-
                      {"","vBC","Base de Cálculo",0},; //2-
                      {"","pPIS","Alíquota",0},; //3-
                      {"","vPIS","Valor",0},; //4-
                      {"","qBCProd","",0},; //5-
                      {"","vAliqProd","",0}}) //6-

         AADD(s_aCOFINS,{{"","CST","CST",2},; //1-
                         {"","vBC","Base de Cálculo",0},; //2-vBC
                         {"","pCOFINS","Alíquota",0},; //3-
                         {"","vCOFINS","Valor",0},; //4-
                         {"","qBCProd","",0},; //5-
                         {"","vAliqProd","",0}}) //6-

     Next

     nIni   := At('<div class="GeralXslt"',cFile)
     cParte := SubStr(cFile, nIni)
     nIni   := At('<div id="Emitente"',cParte)
     cParte := SubStr(cParte, 1, nIni - 1) //ide

     GeraTag("ide", cParte, cFile, cChave)

     nIni   := At('<div id="Emitente"',cFile)
     cParte := SubStr(cFile, nIni)
     nIni   := At('</div>',cParte)
     cParte := SubStr(cParte, 1, nIni - 1) //emit

     GeraTag("emit", cParte, cFile, cChave)

     nIni   := At('<div id="DestRem"',cFile)
     cParte := SubStr(cFile, nIni)
     nIni   := At('</div>',cParte)
     cParte := SubStr(cParte, 1, nIni - 1) //dest

     GeraTag("dest",cParte, cFile, cChave)

     nIni   := At('<td class="fixo-prod-serv-numero"><span>', cFile)
     cParte := SubStr(cFile, nIni)
     nIni   := At('</body></html>', cParte)
     cParte := SubStr(cParte, 1, nIni - 1) //det

     GeraTag("det", cParte, cFile, cChave)

     nIni   := At('<legend class="titulo-aba">Totais</legend>',cFile)
     cParte := SubStr(cFile, nIni+42)
     nIni   := At('<div id="aba_nft_5"',cParte)
     cParte := SubStr(cParte, 1, nIni - 1) //totais

     GeraTag("totais", cParte, cFile, cChave)

     nIni   := At('Dados do Transporte</legend>',cFile)
     cParte := SubStr(cFile, nIni+29)
     nIni   := At('<div id="Cobranca"',cParte)
     cParte := SubStr(cParte, 1, nIni - 1) //transp

     GeraTag("transp", cParte, cFile, cChave)

     nIni   := At('<legend>Duplicatas</legend><table',cFile)

     if nIni > 0
        cParte := SubStr(cFile, nIni)
        nIni   := At('</table>',cParte)
        cParte := SubStr(cParte, 1, nIni - 1) //cobr
        GeraTag("cobr", cParte, cFile, cChave)
     endif

     GravaXML(cChave, hInput, oDialog, lSemParametros, cPasta)

   Else
      MsgInfo('Erro ao ler retorno da Consulta!', 'Atenção!')
      Return .f.
   EndIf

Return .t.

/***********************************************************/
static Function DigitoChave(cCodigoChave)
/*
*/
   Local cIndice := '4329876543298765432987654329876543298765432'
   Local nDigito, nContador, nSoma := 0

   For nContador := 1 to 43
       nSoma += (Val(SubStr(cCodigoChave, nContador, 1)) * Val(SubStr(cIndice, nContador, 1)))
   Next

   nDigito := (nSoma % 11)

   If nDigito == 0 .Or. nDigito == 1
      nDigito := 0
   Else
      nDigito := (11 - nDigito)
   EndIf

   If SubStr(cCodigoChave, 44, 1) != LTrim(Str(nDigito, 1))
     MsgStop('A chave informada é inválida!', 'Atenção!')
     Return .f.
   EndIf

Return .t.

/********************************************************************/
static Function Troca_Acento( cTexto )
/*
*/

   cTexto := StrTran( cTexto, "&#243;", "ó" )
   cTexto := StrTran( cTexto, "&#225;", "á" )
   cTexto := StrTran( cTexto, "&#233;", "é" )

Return cTexto

/*******************************************************************/
static Function GeraTag( _tag, cParte, cFile, cChave )
/*
*/
   Local i, cPar_, c_Tag, nTamTag, nIni, c_span, cPar1_, nFim

   if _tag=="ide"
      cPar_:= StrTran(cFile,Chr(13)+Chr(10),"")
      cPar_:= StrTran(cPar_,Chr(160),"")
      nIni := At('<legend>Informações Complementares de Interesse do Contribuinte</legend>', cPar_)

      if nIni>0
         c_span    := SubStr(cPar_, nIni+70)
         nIni      := AT('<div style="word-wrap: break-word">',c_span)
         c_span    := SubStr(c_span, nIni+35)
         nIni      := AT('</div></span>',c_span)
         s_cinfCpl := alltrim(SubStr(c_span,1,nIni-1))
      endif

      cPar_      := StrTran(cParte,Chr(13)+Chr(10),"")
      cPar_      := StrTran(cPar_,Chr(160),"")

      nIni       := AT('Situação Atual:',cPar_)
      c_span     := SubStr(cPar_, nIni+15)
      nIni       := AT('(Ambiente de autorização:',c_span)
      s_cxMotivo := Alltrim(SubStr(c_span,1,nIni-1))

      if "AUTORIZADA" $ s_cxMotivo
         s_ccStat   := "100"
         s_cxMotivo := "Autorizado o uso da NF-e"
      endif

      if "CANCELADA" $ s_cxMotivo
         s_ccStat   := "101"
         s_cxMotivo := "Cancelamento de NF-e homologado"
      endif

      nIni        := AT('Data Inclusão AN</label></td></tr><tr><td><span>',cPar_)
      c_span      := SubStr(cPar_, nIni+50)
      nIni        := AT('<span>',c_span)+6
      nTamTag     := AT('<input',c_span)-nIni
      s_cnProt    := alltrim(SubStr(c_span,nIni,nTamTag))
      c_span      := SubStr(c_span, nIni+nTamTag)
      nIni        := AT('<span>',c_span)+6
      c_span      := SubStr(c_span,nIni)
      nTamTag     := AT('</span>',c_span)-1 //nIni
      s_cDhRecbto := alltrim(SubStr(c_span,1,nTamTag))
      s_cDhRecbto := SubStr(s_cDhRecbto,7,4)+"-"+SubStr(s_cDhRecbto,4,2)+"-"+left(s_cDhRecbto,2)+"T"+Right(s_cDhRecbto,14)
      nIni        := AT('<i>Digest</i> Value da NF-e',cPar_)
      c_span      := SubStr(cPar_, nIni+20)
      nIni        := AT('<span>',c_span)+6
      nTamTag     := AT('</span>',c_span)-nIni
      s_cDigVal   := alltrim(SubStr(c_span,nIni,nTamTag))

      nIni        := AT('Versão XML',cParte)
      c_span      := SubStr(cParte, nIni)
      nIni        := AT('<span>',c_span)+6
      nTamTag     := AT('</span>',c_span)-nIni
      s_cVersao   := alltrim(SubStr(c_span,nIni,nTamTag))

      For i := 1 to Len(s_aIde[1])

          cPar_:= cParte

          if i == 12
             cPar_:= cFile
          endif

          if i == 13
             cPar_:= cFile
          endif

          nIni   := AT(s_aIde[1,i,5],cPar_)
          c_span := SubStr(cPar_, nIni)

          if i != 16
             nIni     := AT('<span>', c_span) + 6
             nTamTag  := AT('</span>', c_span) - nIni
             c_tag    := alltrim(SubStr(c_span, nIni, nTamTag))
          else
             nIni     := AT('</legend>', c_span)
             c_tag    := alltrim(SubStr(c_span, 1, nIni - 1))
             if AT('produção', c_tag) > 0
                c_tag := "1"
             else
                c_tag := "2"
             endif
             s_cTpAmb := c_tag
          endif

          if i == 2
             c_tag := SubStr(cChave, 36, 8)
             //c_tag:= StrZero(Val(c_tag), 9)
          endif

          if i == 8 .or. i == 9
             if !Empt(c_tag)
                c_tag := SubStr(c_tag,7,4)+"-"+SubStr(c_tag,4,2)+"-"+left(c_tag,2)+"T"+SubStr(c_tag,12)
             endif
          endif

          if i == 15
             c_tag := right(c_tag,1)
          endif

          if s_aIde[1,i,6]!=0
             if s_aIde[1,i,6]>0
                c_tag := left(c_tag,s_aIde[1,i,6])
             else
                c_tag := SubStr(c_tag,Abs(s_aIde[1,i,6])+1)
             endif
          endif

          s_aIde[1,i,1] := c_tag

      Next

   endif

   if _tag == "emit"

      For i := 1 to Len(s_aEmit[1])

          cPar_ := StrTran(cParte,Chr(13)+Chr(10),"")
          cPar_ := StrTran(cPar_,Chr(160),"")
          nIni  := AT(s_aEmit[1,i,5],cPar_)

          if nIni > 0
             c_span  := SubStr(cPar_, nIni)
             nIni    := AT('<span>',c_span)+6
             nTamTag := AT('</span>',c_span)-nIni
             c_tag   := Alltrim(SubStr(c_span,nIni,nTamTag))

             if i >= 1 .and. i <= 2
                c_tag := StrTran(c_tag,".","")
                c_tag := StrTran(c_tag,"/","")
                c_tag := StrTran(c_tag,"-","")
             endif

             if i >= 5 .and. i <= 7

                nIni := AT(",",c_tag)

                if i == 5
                   c_tag := Left(c_tag,nIni-1)
                elseif i == 6
                   c_tag := AllTrim(SubStr(c_tag, nIni + 1))
                   nIni  := AT(" ", c_tag)
                   if nIni > 0
                      c_tag := Left(c_tag, nIni - 1)
                   endif
                else
                   c_tag := AllTrim(SubStr(c_tag, nIni + 1))
                   nIni  := AT(" ", c_tag)
                   if nIni > 0
                      c_tag := alltrim(SubStr(c_tag, nIni + 1))
                   else
                      c_tag := ""
                   endif
                endif
             endif

             if i == 12
                c_tag := StrTran(c_tag,"-","")
             endif

             if i==15
                c_tag := StrTran(c_tag,"(","")
                c_tag := StrTran(c_tag,")","")
                c_tag := StrTran(c_tag,"-","")
             endif

             if s_aEmit[1, i, 6] != 0
                if s_aEmit[1, i, 6] > 0
                   c_tag := left(c_tag,s_aEmit[1, i, 6])
                else
                   c_tag := alltrim(SubStr(c_tag, Abs(s_aEmit[1, i, 6]) + 1))
                   c_tag := alltrim(StrTran(c_tag, "-", ""))
                endif
             endif
             s_aEmit[1, i, 1] := c_tag
          endif
      Next

   endif

   if _tag == "dest"
      For i := 1 to Len(s_aDest[1])
          cPar_ := StrTran(cParte, Chr(13) + Chr(10), "")
          cPar_ := StrTran(cPar_, Chr(160), "")
          nIni  := AT(s_aDest[1, i, 5], cPar_)
          if nIni > 0
             c_span  := SubStr(cPar_, nIni)
             nIni    := AT('<span>',c_span)+6
             nTamTag := AT('</span>',c_span)-nIni
             c_tag   := AllTrim(SubStr(c_span, nIni, nTamTag))
             if i >= 1 .and. i <= 2
                c_tag := StrTran(c_tag, ".", "")
                c_tag := StrTran(c_tag, "/", "")
                c_tag := StrTran(c_tag, "-", "")
             endif
             If i >= 5 .and. i <= 7
                nIni := AT(",", c_tag)
                if i == 5
                   c_tag := Left(c_tag,nIni-1)
                elseif i == 6
                   c_tag := AllTrim(SubStr(c_tag, nIni + 1))
                   nIni  := AT(" ", c_tag)
                   if nIni > 0
                      c_tag := Left(c_tag, nIni - 1)
                   endif
                else
                   c_tag := AllTrim(SubStr(c_tag, nIni + 1))
                   nIni  := AT(" ", c_tag)
                   if nIni > 0
                      c_tag := alltrim(SubStr(c_tag, nIni + 1))
                   else
                      c_tag := ""
                   endif
                endif
             EndIf
             if i == 12
                c_tag := StrTran(c_tag, "-", "")
             endif
             if i == 15
                c_tag := StrTran(c_tag, "(", "")
                c_tag := StrTran(c_tag, ")", "")
                c_tag := StrTran(c_tag, "-", "")
             endif
             if i==16
                c_tag := alltrim(Str(Val(c_tag)))
             endif
             if s_aDest[1, i, 6] != 0
                if s_aDest[1, i, 6] > 0
                   c_tag := left(c_tag, s_aDest[1, i, 6])
                else
                   c_tag := alltrim(SubStr(c_tag, Abs(s_aDest[1,i,6]) + 1))
                   c_tag := alltrim(StrTran(c_tag, "-", ""))
                endif
             endif
             s_aDest[1, i, 1] := c_tag
          endif
      Next

   endif

   if _tag == "det"
      cPar_ := StrTran(cParte, Chr(13) + Chr(10), "")
      cPar_ := StrTran(cPar_, Chr(160), "")
      For p := 1 to Len(s_aDet)
          For i := 1 to Len(s_aDet[p])
              nIni := At(s_aDet[p, i, 3], cPar_)
              if nIni > 0
                 c_span  := SubStr(cPar_, nIni)
                 nIni    := At('<span>',c_span)+6
                 nTamTag := At('</span>',c_span)-nIni
                 c_tag   := AllTrim(SubStr(c_span,nIni,nTamTag))
                 if s_aDet[p, i, 4] != 0
                    if s_aDet[p, i, 4] > 0
                       c_tag := left(c_tag, s_aDet[p, i, 4])
                    else
                       c_tag := alltrim(SubStr(c_tag, Abs(s_aDet[p, i, 4]) + 1))
                       c_tag := alltrim(StrTran(c_tag, "-", ""))
                    endif
                 endif
                 If i != 3
                    c_tag := StrTran(c_tag, ".", "")
                    c_tag := StrTran(c_tag, ",", ".")
                 endIf
                 s_aDet[p, i, 1] := c_tag
              endif
          Next

          For i := 1 To Len(s_aICMS[p])
              nIni := At(s_aICMS[p, i, 3], cPar_)
              if nIni > 0
                 c_span  := SubStr(cPar_, nIni)
                 nIni    := At('<span>', c_span) + 6
                 nTamTag := At('</span>', c_span) - nIni
                 c_tag   := AllTrim(SubStr(c_span, nIni, nTamTag))
                 If s_aICMS[p, i, 4] != 0
                    if s_aICMS[p, i, 4] > 0
                       c_tag := Left(c_tag, s_aICMS[p, i, 4])
                    else
                       c_tag := AllTrim(SubStr(c_tag, Abs(s_aICMS[p, i, 4]) + 1))
                       c_tag := AllTrim(StrTran(c_tag, "-", ""))
                    endif
                 endif
                 c_tag            := StrTran(c_tag, ".", "")
                 c_tag            := StrTran(c_tag, ",", ".")
                 s_aICMS[p, i, 1] := c_tag
              endif
          Next

          nIni := AT('<legend>Imposto Sobre Produtos Industrializados</legend>', cPar_)

          if nIni > 0
             cPar_ := SubStr(cPar_, nIni)
             For i := 1 To Len(s_aIPI[p])
                 nIni := AT(s_aIPI[p, i, 3], cPar_)
                 if nIni > 0
                    c_span  := SubStr(cPar_, nIni)
                    nIni    := AT('<span>', c_span) + 6
                    nTamTag := AT('</span>', c_span) - nIni
                    c_tag   := AllTrim(SubStr(c_span, nIni, nTamTag))
                    if s_aIPI[p, i, 4] != 0
                       if s_aIPI[p, i, 4] > 0
                          c_tag := Left(c_tag, s_aIPI[p, i, 4])
                       else
                          c_tag := Alltrim(SubStr(c_tag, Abs(s_aIPI[p, i, 4]) + 1))
                          c_tag := Alltrim(StrTran(c_tag, "-", ""))
                       endif
                    endif
                    c_tag           := StrTran(c_tag, ".", "")
                    c_tag           := StrTran(c_tag, ",", ".")
                    s_aIPI[p, i, 1] := c_tag
                 endif
             Next
          endif

          nIni := At('<legend class="toggle">PIS</legend>', cPar_)

          if nIni > 0
             cPar_ := SubStr(cPar_, nIni)
             For i := 1 to Len(s_aPIS[p])
                 nFim  := At('<td class="fixo-prod-serv-numero"><span>', cPar_)
                 cPar1_:= SubStr(cPar_, 1, nFim)
                 nIni  := AT(s_aPIS[p, i, 3], cPar1_)
                 if nIni > 0
                    c_span  := SubStr(cPar1_, nIni)
                    nIni    := At('<span>', c_span) + 6
                    nTamTag := At('</span>', c_span) - nIni
                    c_tag   := AllTrim(SubStr(c_span, nIni, nTamTag))
                    if s_aPIS[p,i,4] != 0
                       if s_aPIS[p,i,4] > 0
                          c_tag := Left(c_tag,s_aPIS[p,i,4])
                       else
                          c_tag := AllTrim(SubStr(c_tag, Abs(s_aPIS[p, i, 4]) + 1))
                          c_tag := AllTrim(StrTran(c_tag, "-", ""))
                       endif
                    endif
                    c_tag           := StrTran(c_tag, ".", "")
                    c_tag           := StrTran(c_tag, ",", ".")
                    s_aPIS[p, i, 1] := c_tag
                 endif
             Next
          endif

          nIni := AT('<legend class="toggle">COFINS</legend>', cPar_)

          if nIni > 0
             cPar_ := SubStr(cPar_, nIni)
             For i := 1 to Len(s_aCOFINS[p])
                 nFim  := At('<td class="fixo-prod-serv-numero">', cPar_)
                 cPar1_:= SubStr(cPar_, 1, nFim)
                 nIni  := AT(s_aCOFINS[p, i, 3], cPar1_)
                 if nIni > 0
                    c_span  := SubStr(cPar1_, nIni)
                    nIni    := AT('<span>', c_span) + 6
                    nTamTag := AT('</span>', c_span) - nIni
                    c_tag   := alltrim(SubStr(c_span, nIni, nTamTag))
                    if s_aCOFINS[p, i, 4] != 0
                       if s_aCOFINS[p, i, 4] > 0
                          c_tag := left(c_tag, s_aCOFINS[p, i, 4])
                       else
                          c_tag := alltrim(SubStr(c_tag, Abs(s_aCOFINS[p, i, 4]) + 1))
                          c_tag := alltrim(StrTran(c_tag, "-", ""))
                       endif
                    endif
                    c_tag              := StrTran(c_tag, ".", "")
                    c_tag              := StrTran(c_tag, ",", ".")
                    s_aCOFINS[p, i, 1] := c_tag
                 endif
             Next
          endif

          nIni := At('<legend>Detalhamento específico dos medicamentos</legend>', cPar_)

          if nIni > 0
             cPar_ := SubStr(cPar_, nIni)
             For i := 1 to Len(s_aMedicamentos[p])
                 nIni := AT(s_aMedicamentos[p, i, 3], cPar_)
                 if nIni > 0
                    c_span  := SubStr(cPar_, nIni)
                    nIni    := At('<span>', c_span) + 6
                    nTamTag := At('</span>', c_span) - nIni
                    c_tag   := AllTrim(SubStr(c_span, nIni, nTamTag))
                    if s_aMedicamentos[p,i,4] != 0
                       if s_aMedicamentos[p,i,4] > 0
                          c_tag := Left(c_tag,s_aMedicamentos[p,i,4])
                       else
                          c_tag := AllTrim(SubStr(c_tag, Abs(s_aMedicamentos[p, i, 4]) + 1))
                          c_tag := AllTrim(StrTran(c_tag, "-", ""))
                       endif
                    endif
                    If i != 6
                       c_tag := StrTran(c_tag, ".", "")
                       c_tag := StrTran(c_tag, ",", ".")
                    endif
                    if StrZero(i, 2) $ "-03-04"
                       c_tag:=SubStr(c_tag,7,4)+"-"+SubStr(c_tag,4,2)+"-"+left(c_tag,2)
                    endif
                    s_aMedicamentos[p, i, 1] := c_tag
                 endif
             Next
          endif

          nIni  := At('<td class="fixo-prod-serv-numero"><span>', cPar_)
          cPar_ := SubStr(cPar_, nIni)

      Next

   endif

   if _tag=="totais"
      For i := 1 to Len(s_aTotais[1])
          cPar_ := StrTran(cParte,Chr(13)+Chr(10),"")
          cPar_ := StrTran(cPar_,Chr(160),"")
          nIni  := AT(s_aTotais[1,i,3],cPar_)
          if nIni > 0
             c_span           := SubStr(cPar_, nIni)
             nIni             := AT('<span>',c_span)+6
             nTamTag          := AT('</span>',c_span)-nIni
             c_tag            := alltrim(SubStr(c_span,nIni,nTamTag))
             c_tag            := StrTran(c_tag,".","")
             c_tag            := StrTran(c_tag,",",".")
             s_aTotais[1,i,1] := c_tag
          endif
      Next
   endif

   if _tag=="transp"
      For i := 1 to Len(s_aTransp[1])
          cPar_ := StrTran(cParte,Chr(13)+Chr(10),"")
          cPar_ := StrTran(cPar_,Chr(160),"")
          nIni  := AT(s_aTransp[1,i,5],cPar_)
          if nIni > 0
             c_span  := SubStr(cPar_, nIni)
             nIni    := AT('<span>',c_span)+6
             nTamTag := AT('</span>',c_span)-nIni
             c_tag   := alltrim(SubStr(c_span,nIni,nTamTag))
             if i >= 2 .and. i <= 3
                c_tag := StrTran(c_tag,".","")
                c_tag := StrTran(c_tag,"/","")
                c_tag := StrTran(c_tag,"-","")
             endif
             if s_aTransp[1,i,6] != 0
                if s_aTransp[1,i,6]>0
                   c_tag := left(c_tag,s_aTransp[1,i,6])
                else
                   c_tag := alltrim(SubStr(c_tag,Abs(s_aTransp[1,i,6])+1))
                   c_tag := alltrim(StrTran(c_tag,"-",""))
                endif
             endif
             s_aTransp[1,i,1] := c_tag
          endif
      Next
   endif

   if _tag == "cobr"
      cPar_ := StrTran(cParte,Chr(13)+Chr(10),"")
      cPar_ := StrTran(cPar_,Chr(160),"")
      For i := 1 to Len(s_aCobr[1])
          nIni := AT('<span>',cPar_)
          if nIni > 0
             cPar_   := SubStr(cPar_, nIni+6)
             nTamTag := AT('</span>',cPar_)-1
             c_tag   := alltrim(SubStr(cPar_,1,nTamTag))
             c_tag   := StrTran(c_tag,".","")
             c_tag   := StrTran(c_tag,",",".")
             if StrZero(i,2) $ "-02-05-08-11-14-17-20-23-26-29"
                c_tag := SubStr(c_tag,7,4)+"-"+SubStr(c_tag,4,2)+"-"+left(c_tag,2)
             endif
             s_aCobr[1,i,1] := c_tag
          else
             Exit
          endif
      Next
   endif

return Nil

/**************************************************************************************************/
static function GravaXML(cChave, hInput, oDialog, lSemParametros, cPasta)
/*
*/
   Local i, p, lTemTag, cTagPai, nFimTagPai, nIni, cTag, nArqHandle, cArqXml, cTag_
   Local cXML :='<?xml version="1.0" encoding="UTF-8"?><nfeProc versao="'+s_cVersao+'" xmlns="http://www.portalfiscal.inf.br/nfe"><NFe xmlns="http://www.portalfiscal.inf.br/nfe"><infNFe versao="'+s_cVersao+'" Id="NFe'+cChave+'">'

   cTagPai    := ""
   nFimTagPai := 0

   For i := 1 to Len(s_aIde[1])
       if !Empt(s_aIde[1,i,3])
          nFimTagPai := s_aIde[1,i,4]
          cTagPai    += "|"+StrZero(nFimTagPai,2)+s_aIde[1,i,3]
          cXML       += '<'+s_aIde[1,i,3]+'>'
       endif
       if !Empty(s_aIde[1,i,1])
          cXML += '<'+s_aIde[1,i,2]+'>'+s_aIde[1,i,1]+'</'+s_aIde[1,i,2]+'>'
       endif
       if i == nFimTagPai
          nIni    := RAT("|",cTagPai)
          cTag    := Substr(cTagPai,nIni+1)
          cXML    += '</'+SubStr(cTag,3)+'>'
          cTagPai := SubStr(cTagPai,1,nIni-1)
          if nIni > 1
             nIni       := RAT("|",cTagPai)
             nFimTagPai := Val(Substr(cTagPai,nIni+1,2))
          else
             nFimTagPai := 0
          endif
       endif
   Next

   For i := 1 to Len(s_aEmit[1])
       if !Empty(s_aEmit[1,i,3])
          nFimTagPai := s_aEmit[1,i,4]
          cTagPai    += "|"+StrZero(nFimTagPai,2)+s_aEmit[1,i,3]
          cXML       += '<'+s_aEmit[1,i,3]+'>'
       endif
       if !Empty(s_aEmit[1,i,1])
          cXML += '<'+s_aEmit[1,i,2]+'>'+s_aEmit[1,i,1]+'</'+s_aEmit[1,i,2]+'>'
       endif
       if i == nFimTagPai
          nIni    := RAT("|",cTagPai)
          cTag    := Substr(cTagPai,nIni+1)
          cXML    += '</'+SubStr(cTag,3)+'>'
          cTagPai := SubStr(cTagPai,1,nIni-1)
          if nIni > 1
             nIni       := RAT("|",cTagPai)
             nFimTagPai := Val(Substr(cTagPai,nIni+1,2))
          else
             nFimTagPai := 0
          endif
       endif
   Next

   For i := 1 to Len(s_aDest[1])
       if !Empt(s_aDest[1,i,3])
          nFimTagPai := s_aDest[1,i,4]
          cTagPai    += "|"+StrZero(nFimTagPai,2)+s_aDest[1,i,3]
          cXML       += '<'+s_aDest[1,i,3]+'>'
       endif
       if !Empt(s_aDest[1,i,1])
          cXML += '<'+s_aDest[1,i,2]+'>'+s_aDest[1,i,1]+'</'+s_aDest[1,i,2]+'>'
       endif
       if i == nFimTagPai
          nIni    := RAT("|",cTagPai)
          cTag    := Substr(cTagPai,nIni+1)
          cXML    += '</'+SubStr(cTag,3)+'>'
          cTagPai := SubStr(cTagPai,1,nIni-1)
          if nIni > 1
             nIni       := RAT("|",cTagPai)
             nFimTagPai := Val(Substr(cTagPai,nIni+1,2))
          else
             nFimTagPai := 0
          endif
       endif
   Next

   For p := 1 to Len(s_aDet)
       cXML += '<det nItem="'+alltrim(Str(p))+'"><prod>'
       For i := 1 to Len(s_aDet[p])
           if !Empt(s_aDet[p,i,1])
              cXML += '<'+s_aDet[p,i,2]+'>'+s_aDet[p,i,1]+'</'+s_aDet[p,i,2]+'>'
           endif
       Next

       lTemTag := .F.
       For i := 1 to Len(s_aMedicamentos[p])
           if i == 1
              if !Empty(s_aMedicamentos[p,i,1])
                 cXML    += '<med><nLote>' + s_aMedicamentos[p,i,1] + '</nLote>'
                 lTemTag := .t.
              endif
           Else
              if !Empty(s_aMedicamentos[p,i,1])
                 If i == 6
                    s_cDiscAdicional := '<'+s_aMedicamentos[p,i,2]+'>'+ s_aMedicamentos[p,i,1] + '</'+s_aMedicamentos[p,i,2]+'>'
                 Else
                    cXML             += '<'+s_aMedicamentos[p,i,2]+'>'+ s_aMedicamentos[p,i,1] + '</'+s_aMedicamentos[p,i,2]+'>'
                 Endif
              endif
           EndIf
       Next

       if lTemTag
          cXML += '</med>'
       endif

       cXML  += '</prod><imposto><ICMS>'
       cTag_ := 'ICMS'

       For i := 1 to Len(s_aICMS[p])
           if i == 1
              if !Empty(s_aICMS[p, 2, 1])
                 cTag_ += s_aICMS[p, 2, 1]
              else
                 cTag_ += 'SN' + s_aICMS[p, 3, 1]
              endif
              cXML += '<' + cTag_ + '>'
           endif
           if !Empty(s_aICMS[p, i, 1])
              If s_aICMS[p, 2, 1] == '00'
                 If StrZero(i, 2) $ "-01-02-04-06-07-08"
                    cXML += '<' + s_aICMS[p, i, 2] + '>' + s_aICMS[p, i, 1] + '</' + s_aICMS[p, i, 2] + '>'
                 EndIf
              ElseIf s_aICMS[p, 2, 1] == '10'
                 If StrZero(i, 2) $ "-01-02-04-06-07-08-11-12-13-14-15-16"
                    cXml += '<' + s_aICMS[p, i, 2] + '>' + s_aICMS[p, i, 1] + '</' + s_aICMS[p, i, 2] + '>'
                 EndIf
              ElseIf s_aICMS[p, 2, 1] == '20'
                 If StrZero(i, 2) $ "-01-02-04-05-06-07-08-09-10"
                    cXml += '<' + s_aICMS[p, i, 2] + '>' + s_aICMS[p, i, 1] + '</' + s_aICMS[p, i, 2] + '>'
                 EndIf
              ElseIf s_aICMS[p, 2, 1] == '30'
                 If StrZero(i, 2) $ "-01-02-11-12-13-14-15-16-09-10"
                    cXml += '<' + s_aICMS[p, i, 2] + '>' + s_aICMS[p, i, 1] + '</' + s_aICMS[p, i, 2] + '>'
                 EndIf
              ElseIf s_aICMS[p, 2, 1] == '40' .or. s_aICMS[p, 2, 1] == '41' .Or. s_aICMS[p, 2, 1] == '50'
                 If StrZero(i, 2) $ "-01-02-09-10"
                    cXml += '<' + s_aICMS[p, i, 2] + '>' + s_aICMS[p, i, 1] + '</' + s_aICMS[p, i, 2] + '>'
                 EndIf
              ElseIf s_aICMS[p, 2, 1] == '51'
                 If StrZero(i, 2) $ "-01-02-04-05-06-07-08-19-20-21"
                    cXml += '<' + s_aICMS[p, i, 2] + '>' + s_aICMS[p, i, 1] + '</' + s_aICMS[p, i, 2] + '>'
                 EndIf
              ElseIf s_aICMS[p, 2, 1] == '60'
                 If StrZero(i, 2) $ "-01-02-17-18"
                    cXml += '<' + s_aICMS[p, i, 2] + '>' + s_aICMS[p, i, 1] + '</' + s_aICMS[p, i, 2] + '>'
                 EndIf
              ElseIf s_aICMS[p, 2, 1] == '70'
                 If StrZero(i, 2) $ "-01-02-04-05-06-07-08-09-10-11-12-13-14-15-16"
                    cXml += '<' + s_aICMS[p, i, 2] + '>' + s_aICMS[p, i, 1] + '</' + s_aICMS[p, i, 2] + '>'
                 EndIf
              ElseIf s_aICMS[p, 2, 1] == '90'
                 If StrZero(i, 2) $ "-01-02-04-05-06-07-08-11-12-13-14-15-16"
                    cXml += '<' + s_aICMS[p, i, 2] + '>' + s_aICMS[p, i, 1] + '</' + s_aICMS[p, i, 2] + '>'
                 EndIf
              Else
                 cXml += '<' + s_aICMS[p, i, 2] + '>' + s_aICMS[p, i, 1] + '</' + s_aICMS[p, i, 2] + '>'
              EndIf
           endif
       Next

       cXML    += '</'+cTag_+'></ICMS>'
       lTemTag := .F.

       For i := 1 to Len(s_aIPI[p])
           if i == 1
              if !Empty(s_aIPI[p,i,1])
                 cXML    += '<IPI><cEnq>' + s_aIPI[p,i,1] + '</cEnq><IPITrib>'
                 lTemTag := .T.
              endif
           Else
              if !Empty(s_aIPI[p,i,1])
                 cXML += '<'+s_aIPI[p,i,2]+'>'+ s_aIPI[p,i,1] + '</'+s_aIPI[p,i,2]+'>'
              endif
           EndIf
       Next

       if lTemTag
          cXML += '</IPITrib></IPI>'
       endif

       lTemTag := .F.

       For i:= 1 to Len(s_aPIS[p])
           if i == 1
              if !Empty(s_aPIS[p, i, 1])
                 If Val(s_aPIS[p, 1, 1]) >= 1 .And. Val(s_aPIS[p, 1, 1]) <= 2
                    cXML += '<PIS><PISAliq>'
                 ElseIf Val(s_aPIS[p, 1, 1]) == 3
                    cXML += '<PIS><PISQte>'
                 ElseIf Val(s_aPIS[p, 1, 1]) >= 4 .And. Val(s_aPIS[p, 1, 1]) <= 9
                    cXML += '<PIS><PISNT>'
                 ElseIf Val(s_aPIS[p, 1, 1]) >= 49 .And. Val(s_aPIS[p, 1, 1]) <= 99
                    cXML += '<PIS><PISOutr>'
                 EndIf
                 lTemTag := .T.
              endif
           endif
           if !Empty(s_aPIS[p, i, 1])
              cXML += '<' + s_aPIS[p,i,2] + '>' + s_aPIS[p,i,1] + '</' + s_aPIS[p,i,2] + '>'
           endif
       Next

       if lTemTag
          If Val(s_aPIS[p, 1, 1]) >= 1 .And. Val(s_aPIS[p, 1, 1]) <= 2
             cXML += '</PISAliq></PIS>'
          ElseIf Val(s_aPIS[p, 1, 1]) == 3
             cXML += '</PISQte></PIS>'
          ElseIf Val(s_aPIS[p, 1, 1]) >= 4 .And. Val(s_aPIS[p, 1, 1]) <= 9
             cXML += '</PISNT></PIS>'
          ElseIf Val(s_aPIS[p, 1, 1]) >= 49 .And. Val(s_aPIS[p, 1, 1]) <= 99
             cXML+= '</PISOutr></PIS>'
          EndIf
       endif

       lTemTag := .F.

       For i := 1 to Len(s_aCOFINS[p])
           if i == 1
              if !Empty(s_aCOFINS[p,i,1])
                 If Val(s_aPIS[p, 1, 1]) >= 1 .And. Val(s_aPIS[p, 1, 1]) <= 2
                    cXML += '<COFINS><COFINSAliq>'
                 ElseIf Val(s_aPIS[p, 1, 1]) == 3
                    cXML += '<COFINS><COFINSQte>'
                 ElseIf Val(s_aPIS[p, 1, 1]) >= 4 .And. Val(s_aPIS[p, 1, 1]) <= 9
                    cXML += '<COFINS><COFINSNT>'
                 ElseIf Val(s_aPIS[p, 1, 1]) >= 49 .And. Val(s_aPIS[p, 1, 1]) <= 99
                    cXML += '<COFINS><COFINSOutr>'
                 EndIf
                 lTemTag := .T.
              endif
           endif
           if !Empty(s_aCOFINS[p,i,1])
              cXML += '<' + s_aCOFINS[p, i, 2] + '>' + s_aCOFINS[p, i, 1] + '</' + s_aCOFINS[p, i, 2] + '>'
           endif
       Next

       if lTemTag
          If Val(s_aPIS[p, 1, 1]) >= 1 .And. Val(s_aPIS[p, 1, 1]) <= 2
             cXML += '</COFINSAliq></COFINS>'
          ElseIf Val(s_aPIS[p, 1, 1]) == 3
             cXML += '</COFINSQte></COFINS>'
          ElseIf Val(s_aPIS[p, 1, 1]) >= 4 .And. Val(s_aPIS[p, 1, 1]) <= 9
             cXML += '</COFINSNT></COFINS>'
          ElseIf Val(s_aPIS[p, 1, 1]) >= 49 .And. Val(s_aPIS[p, 1, 1]) <= 99
             cXML+= '</COFINSOutr></COFINS>'
          EndIf
       endif
       cXML += '</imposto>'
       If !Empty(s_cDiscAdicional)
          cXml += s_cDiscAdicional
       EndIf
       cXML += '</det>'
   Next

   cXML += '<total><ICMSTot>'

   For i := 1 to Len(s_aTotais[1])
       if !Empty(s_aTotais[1,i,1])
          cXML+='<'+s_aTotais[1,i,2]+'>'+s_aTotais[1,i,1]+'</'+s_aTotais[1,i,2]+'>'
       endif
   Next

   cXML += '</ICMSTot></total>'

   For i := 1 to Len(s_aTransp[1])
       if !Empty(s_aTransp[1,i,3])
          nFimTagPai := s_aTransp[1,i,4]
          cTagPai    += "|"+StrZero(nFimTagPai,2)+s_aTransp[1,i,3]
          cXML       += '<'+s_aTransp[1,i,3]+'>'
       endif
       if !Empty(s_aTransp[1,i,1])
          cXML += '<'+s_aTransp[1,i,2]+'>'+s_aTransp[1,i,1]+'</'+s_aTransp[1,i,2]+'>'
       endif
       if i == nFimTagPai
          nIni    := RAT("|",cTagPai)
          cTag    := Substr(cTagPai,nIni+1)
          cXML    += '</'+SubStr(cTag,3)+'>'
          cTagPai := SubStr(cTagPai,1,nIni-1)
          if nIni > 1
             nIni       := RAT("|",cTagPai)
             nFimTagPai := Val(Substr(cTagPai,nIni+1,2))
          else
             nFimTagPai := 0
          endif
          if SubStr(cTag,3) == "vol"
             cXML += '</transp>'
          endif
       endif
   Next

   if !Empty(s_aCobr[1,1,1])
      cXML += '<cobr>'
      For i := 1 to Len(s_aCobr[1])
          if !Empty(s_aCobr[1,i,1])
             if StrZero(i,2) $ "-01-04-07-10-13-16-19-22-25-28"
                if i > 2
                   cXML += '</dup>'
                endif
                cXML += '<dup>'
             endif
             cXML += '<'+s_aCobr[1,i,2]+'>'+s_aCobr[1,i,1]+'</'+s_aCobr[1,i,2]+'>'
          endif
      Next
      cXML += '</dup></cobr>'
   endif

   if !Empty(s_cinfCpl) .or. !Empty(s_cInfAdFisco)
      cXML += '<infAdic>'
      if !Empty(s_cInfAdFisco)
         cXML += '<infAdFisco>'+s_cInfAdFisco+'</infAdFisco>'
      endif
      if !Empty(s_cinfCpl)
         cXML += '<infCpl>'+s_cinfCpl+'</infCpl>'
      endif
      cXML += '</infAdic>'
   endif

   cXML       += '</infNFe>'
   cXML       += '<Signature xmlns="http://www.w3.org/2000/09/xmldsig#"><SignedInfo><CanonicalizationMethod Algorithm="http://www.w3.org/TR/2001/REC-xml-c14n-20010315"/><SignatureMethod Algorithm="http://www.w3.org/2000/09/xmldsig#rsa-sha1" />'
   cXML       += '<Reference URI="#NFe'+cChave+'">'
   cXML       += '<Transforms><Transform Algorithm="http://www.w3.org/2000/09/xmldsig#enveloped-signature" /><Transform Algorithm="http://www.w3.org/TR/2001/REC-xml-c14n-20010315" /></Transforms><DigestMethod Algorithm="http://www.w3.org/2000/09/xmldsig#sha1" />'
   cXML       += '<DigestValue>'+s_cDigVal+'</DigestValue></Reference></SignedInfo><SignatureValue></SignatureValue><KeyInfo></KeyInfo></Signature></NFe>'
   cXML       += '<protNFe versao="'+s_cVersao+'"><infProt>'
   cXML       += '<tpAmb>'+s_cTpAmb+'</tpAmb><verAplic>'+s_cVerAplic+'</verAplic><chNFe>'+cChave+'</chNFe><dhRecbto>'+s_cDhRecbto+'</dhRecbto>'
   cXML       += '<nProt>'+s_cnProt+'</nProt><digVal>'+s_cDigVal+'</digVal><cStat>'+s_ccStat+'</cStat><xMotivo>'+s_cxMotivo+'</xMotivo>'
   cXML       += '</infProt></protNFe></nfeProc>'

   cArqXml    := cPasta + '\' + cChave + '.xml'
   nArqHandle := FCreate(cArqXml, 0 )

   FWrite(nArqHandle,cXML)
   FClose(nArqHandle)

   MsgInfo( 'Download realizado com sucesso! Arquivo gerado:' + CRLF + cArqXml, 'NF-e ')

   if lSemParametros
      AtualizaCaptcha( hInput )

      hInput['Chave'  ]:cText( Space( 44 ) )
      hInput['Captcha']:cText( Space(  6 ) )
      hInput['Chave'  ]:setFocus()
   else
      oDialog:end()
   endif

return nil